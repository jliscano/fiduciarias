/*****
* CONFIGURATION
*/
    //Main navigation
    $.navigation = $('nav > ul.nav');

  $.panelIconOpened = 'icon-arrow-up';
  $.panelIconClosed = 'icon-arrow-down';

  //Default colours
  $.brandPrimary =  '#20a8d8';
  $.brandSuccess =  '#4dbd74';
  $.brandInfo =     '#63c2de';
  $.brandWarning =  '#f8cb00';
  $.brandDanger =   '#f86c6b';

  $.grayDark =      '#2a2c36';
  $.gray =          '#55595c';
  $.grayLight =     '#818a91';
  $.grayLighter =   '#d1d4d7';
  $.grayLightest =  '#f8f9fa';

'use strict';

/****
* MAIN NAVIGATION
*/

$(document).ready(function($){

  // Add class .active to current link
  $.navigation.find('a').each(function(){

    var cUrl = String(window.location).split('?')[0];

    if (cUrl.substr(cUrl.length - 1) == '#') {
      cUrl = cUrl.slice(0,-1);
    }

    if ($($(this))[0].href==cUrl) {
      $(this).addClass('active');

      $(this).parents('ul').add(this).each(function(){
        $(this).parent().addClass('open');
      });
    }
  });

  // Dropdown Menu
  $.navigation.on('click', 'a', function(e){

    if ($.ajaxLoad) {
      e.preventDefault();
    }

    if ($(this).hasClass('nav-dropdown-toggle')) {
      $(this).parent().toggleClass('open');
      resizeBroadcast();
    }

  });

  function resizeBroadcast() {

    var timesRun = 0;
    var interval = setInterval(function(){
      timesRun += 1;
      if(timesRun === 5){
        clearInterval(interval);
      }
      window.dispatchEvent(new Event('resize'));
    }, 62.5);
  }

  /* ---------- Main Menu Open/Close, Min/Full ---------- */
  $('.sidebar-toggler').click(function(){
    $('body').toggleClass('sidebar-hidden');
    resizeBroadcast();
  });

  $('.sidebar-minimizer').click(function(){
    $('body').toggleClass('sidebar-minimized');
    resizeBroadcast();
  });

  $('.brand-minimizer').click(function(){
    $('body').toggleClass('brand-minimized');
  });

  $('.aside-menu-toggler').click(function(){
    $('body').toggleClass('aside-menu-hidden');
    resizeBroadcast();
  });

  $('.mobile-sidebar-toggler').click(function(){
    $('body').toggleClass('sidebar-mobile-show');
    resizeBroadcast();
  });

  $('.sidebar-close').click(function(){
    $('body').toggleClass('sidebar-opened').parent().toggleClass('sidebar-opened');
  });

  /* ---------- Disable moving to top ---------- */
  $('a[href="#"][data-top!=true]').click(function(e){
    e.preventDefault();
  });

});

/****
* CARDS ACTIONS
*/

$(document).on('click', '.card-actions a', function(e){
  e.preventDefault();

  if ($(this).hasClass('btn-close')) {
    $(this).parent().parent().parent().fadeOut();
  } else if ($(this).hasClass('btn-minimize')) {
    var $target = $(this).parent().parent().next('.card-block');
    if (!$(this).hasClass('collapsed')) {
      $('i',$(this)).removeClass($.panelIconOpened).addClass($.panelIconClosed);
    } else {
      $('i',$(this)).removeClass($.panelIconClosed).addClass($.panelIconOpened);
    }

  } else if ($(this).hasClass('btn-setting')) {
    $('#myModal').modal('show');
  }

});

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function init(url) {

  /* ---------- Tooltip ---------- */
  $('[rel="tooltip"],[data-rel="tooltip"]').tooltip({"placement":"bottom",delay: { show: 400, hide: 200 }});

  /* ---------- Popover ---------- */
  $('[rel="popover"],[data-rel="popover"],[data-toggle="popover"]').popover();

}





$(document).ready( function () {
  $('#bacth').DataTable(

{
       "order": [[ 1, "desc" ]],
      "language": {
          "url": "/json/Spanish.json"
      }
  },



    );



} );

$(document).ready( function () {
  $('#bacth2').DataTable(

{
       "order": [[ 8, "asc" ]],
      "language": {
          "url": "/json/Spanish.json"
      }
  },
{
  buttons: [
      'copy', 'excel', 'pdf'
  ]
}


    );



} );







$(document).ready(function() {
  var
      gradeTable = $('#grade-table'),
      gradeBody = gradeTable.find('tbody'),
      gradeTemplate = _.template($('#grade-template').remove().text()),
      numberRows = gradeTable.find('tbody > tr').length;

  gradeTable
      .on('click', 'a.add', function(e) {
          e.preventDefault();

          $(gradeTemplate({key: numberRows++}))
              .hide()
              .appendTo(gradeBody)
              .fadeIn('fast');
      })
      .on('click', 'a.remove', function(e) {
              e.preventDefault();

          $(this)
              .closest('tr')
              .fadeOut('fast', function() {
                  $(this).remove();
              });
      });

      if (numberRows === 0) {
          gradeTable.find('a.add').click();
      }
});



$(document).ready(function() {
  var
      gradeTable = $('#grade-table2'),
      gradeBody = gradeTable.find('tbody'),
      gradeTemplate = _.template($('#grade-template2').remove().text()),
      numberRows = gradeTable.find('tbody > tr').length;

  gradeTable
      .on('click', 'a.add', function(e) {
          e.preventDefault();

          $(gradeTemplate({key: numberRows++}))
              .hide()
              .appendTo(gradeBody)
              .fadeIn('fast');
      })
      .on('click', 'a.remove', function(e) {
              e.preventDefault();

          $(this)
              .closest('tr')
              .fadeOut('fast', function() {
                  $(this).remove();
              });
      });

      if (numberRows === 0) {
          gradeTable.find('a.add').click();
      }
});










var opcionescuentas = {
  url: "/cuentas/simple.json",

    getValue: function(element) {
        return element.numero;
    },

    list: {
        onSelectItemEvent: function() {
            var selectedItemValue = $("#cuentasbancarias").getSelectedItemData().id;

            $("#cuenta_id").val(selectedItemValue).trigger("change");
        },
        match: {
            enabled: true
        }
    }
  };

$("#cuentasbancarias").easyAutocomplete(opcionescuentas);


$(document).ready(function(){
    //group add limit
    var maxGroup = 2;

    //add more fields group
    $(".addMore").click(function(){
        if($('body').find('.fieldGroup').length < maxGroup){
            var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
            $('body').find('.fieldGroup:last').after(fieldHTML);
        }else{
            alert('Sólo se permite un máximo de'+maxGroup+' movimientos.');
        }
    });

    //remove fields group
    $("body").on("click",".remove",function(){
        $(this).parents(".fieldGroup").remove();
    });
});





  $(function () {
    $('#datetimepicker4').datetimepicker({
                    format: 'YYYY/MM/DD'
                });
});




var room = 1;
function education_fields() {

    room++;
    var objTo = document.getElementById('education_fields')
    var divtest = document.createElement("div");
	divtest.setAttribute("class", "form-group removeclass"+room);
	var rdiv = 'removeclass'+room;
    divtest.innerHTML = '  <span><?php echo "Hola";?> ajá</span>';

    objTo.appendChild(divtest)
}
   function remove_education_fields(rid) {
	   $('.removeclass'+rid).remove();
   }



$('#tokenize1').tokenize2();

$('#tokenize-sortable1').tokenize2({
    sortable: true
});

$('#gggg').tokenize2({
    sortable: true,
    tokensMaxItems: 1,

});

$(document).ready(function () {
  $("#cuenta-id").bind("change",
  function (event) {
    $.ajax({
  async:true,
  data: $("#cuenta-id").serialize(),
  dataType:"html",
  success:
  function (data, textStatus) {
    $("#cliente-id").html(data);
    },
    type:"post", url:"\/operaciones\/clientes"});
return false;
    });
});

//Buscar por fecha





$(document).ready(function(){

var fullDate = new Date();
var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) :(fullDate.getMonth()+1);

var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();

  var table = $('#tabla-reportes').DataTable(


          {

                     "dom": "t",
                    "language": {
                        "url": "/json/Spanish.json"
                    },
                    dom: 'Bfrtip',
                    buttons: [
                      { extend: 'copy', text: 'Copiar' },
                      { extend: 'csv', text: 'CSV' },
                      { extend: 'excel', text: 'Excel' },
                      { extend: 'pdf', text: 'PDF',  messageTop: 'Fecha:' + currentDate, title: 'Trust Services, S.A.' },
                      { extend: 'print', text: 'Imprimir', messageTop: 'Fecha:' + currentDate, title: 'Trust Services, S.A.' }
                  ]


      });
      var myTable = $('#tabla-reportes').DataTable();

    yadcf.init(myTable, [
    {column_number : 0, filter_type: "auto_complete", filter_default_label: "Batch", filter_container_id: "batchre"},
    {column_number : 1, filter_default_label: "Cuenta", filter_type: "auto_complete", filter_container_id: "cuentare"},
    {column_number : 2, column_data_type: "html", html_data_type: "text", filter_default_label: "Banco", filter_container_id: "bancore"},
    {column_number : 3, filter_type: "auto_complete", text_data_delimiter: ",", filter_default_label: "Cliente", filter_container_id: "clientere"},
    {column_number : 4,  filter_type: "auto_complete", filter_default_label: "Monto", filter_container_id: "montore"},
    {column_number : 5, column_data_type: "html", html_data_type: "text", filter_default_label: "Moneda", filter_container_id: "monedare"},
    {column_number : 6, filter_type: "auto_complete", text_data_delimiter: ",", filter_default_label: "Descripción", filter_container_id: "descripcionre"},
    {column_number : 7, column_data_type: "html", html_data_type: "text", filter_default_label: "Transacción", filter_container_id: "transaccionre"},
    {column_number : 8, filter_type: "range_date",  filter_default_label: ["Desde", "Hasta"], filter_container_id: "fechare", date_format: "dd-mm-yy"}


  ]);


    });




      var table = $('#table_id').DataTable(

        {

              "language": {
                  "url": "/json/Spanish.json"
              },
              dom: 'Bfrtip',

              buttons: [
                { extend: 'copy', text: 'Copiar' },
                { extend: 'csv', text: 'CSV' },
                { extend: 'excel', text: 'Excel' },
                { extend: 'pdf', text: 'PDF' },
                { extend: 'print', text: 'Imprimir' }
            ]



});

var table = $('#cuentas').DataTable(

  {

        "language": {
            "url": "/json/Spanish.json"
        },
        dom: 'Bfrtip',

        buttons: [
          { extend: 'copy', text: 'Copiar',  exportOptions: {
            columns: [ 0, 1, 2, 3, 4,  5, 6, 7 ]
        } },
          { extend: 'csv', text: 'CSV',  exportOptions: {
            columns: [ 0, 1, 2, 3, 4,  5, 6, 7 ]    } },
          { extend: 'excel', text: 'Excel', title: 'Cuentas',  exportOptions: {
            columns: [ 0, 1, 2, 3, 4,  5, 6, 7 ]    } },
          { extend: 'pdf', text: 'PDF',  title: 'Cuentas', exportOptions: {
            columns: [ 0, 1, 2, 3, 4,  5, 6, 7 ]    } },
          { extend: 'print', text: 'Imprimir', title: 'Cuentas',  exportOptions: {
            columns: [ 0, 1, 2, 3, 4,  5, 6, 7 ]    } }
      ]



});

var table = $('#clientes').DataTable(

  {

        "language": {
            "url": "/json/Spanish.json"
        },
        dom: 'Bfrtip',

        buttons: [
          { extend: 'copy', text: 'Copiar',  exportOptions: {
            columns: [ 0, 1, 2, 3, 4,  5 ]
        } },
          { extend: 'csv', text: 'CSV',  exportOptions: {
            columns: [ 0, 1, 2, 3, 4,  5 ]    } },
          { extend: 'excel', text: 'Excel',  title: 'Clientes', exportOptions: {
            columns: [ 0, 1, 2, 3, 4,  5 ]    } },
          { extend: 'pdf', text: 'PDF',  title: 'Clientes', exportOptions: {
            columns: [ 0, 1, 2, 3, 4,  5 ]    } },
          { extend: 'print', text: 'Imprimir',  title: 'Clientes',  exportOptions: {
            columns: [ 0, 1, 2, 3, 4,  5 ]    } }
      ]



});
