-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: fiduciaria
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bancos`
--

DROP TABLE IF EXISTS `bancos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bancos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `paise_id` int(11) NOT NULL,
  `direccion` text NOT NULL,
  `telefono` varchar(120) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `paise_id` (`paise_id`),
  CONSTRAINT `bancos_ibfk_1` FOREIGN KEY (`paise_id`) REFERENCES `paises` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bancos`
--

LOCK TABLES `bancos` WRITE;
/*!40000 ALTER TABLE `bancos` DISABLE KEYS */;
INSERT INTO `bancos` VALUES (1,'BANISTMO',1,'LOCAL','2636200',1,'2017-07-25 22:35:32','2017-08-24 18:07:48'),(2,'BANCO MERCANTIL DEL ISTMO',1,'Local','263-7181',1,'2018-02-03 12:29:57','2018-02-03 12:29:57'),(3,'SWISS BANK CORPORATION',1,'Local','2637281',1,'2018-02-03 12:30:25','2018-02-03 12:30:25'),(4,'BANCO BILBAO VIZCAYA',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(5,'BANCO INTL COSTA RICA-MIAMI',1,'Local','263-6822',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(6,'BANCO DISA',1,'Local','11',0,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(7,'PANABANK',1,'Local','263-9266',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(8,'BANCO DO BRAZIL',1,'Local','263-6566',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(9,'BANCO ATLANTICO',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(10,'CITIBANK - MIAMI',2,'Extranjera','11',0,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(11,'BANQUE SCS ALLIANCE (NASSAU)',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(12,'ABN - AMRO BANK - MIAMI',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(13,'BANCO GENERAL',1,'Local','264.0674',0,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(14,'BANISTMO - T.S.(CTA.TRUST)',1,'local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(15,'SWISS BANK - TRUST SERVICES',1,'local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(16,'BANCO DISA-PLAZO FIJO',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(17,'ABN-AMRO BANK/PANAMA',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(18,'WALL STREET SECURITIES - PMA.',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(19,'ABN-AMRO BANK- MIAMI',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(20,'SWISS BANK - PLAZO FIJO',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(21,'BANCO DO BRAZIL - PLAZO FIJO',1,'Local','263.6566',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(22,'CITIBANK-MIAMI',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(23,'ABN-AMRO BANK ( \"OVERNIGHT\" )',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(24,'PRIMER BCO.ISTMO (OVERNIGHT)',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(25,'HSBC-BANK',1,'Local','11',0,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(26,'CITIBANK - PANAMA',1,'local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(27,'CITIBANK - PANAMA',1,'local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(28,'HSBC-BANK plc. - OVERNIGHT',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(29,'PRIMER BANCO DEL ISTMO (P.F.)',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(30,'HSBC-BANK plc. - CAYMAN',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(31,'BBL-BANQUE BRUXELLES LAMBERT',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(32,'BANCO DISA',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(33,'CREDICORP BANK',1,'PLAZA CREDICORP BANK PANAMA CALLE 5','11',0,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(34,'CREDICORP BANK (OVERNIGHT)',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(35,'BANCO DISA - T.S. (CTA.TRUST)',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(36,'PAINE WEBBER',1,'Local','FAX 3055369116',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(37,'SAFRA NATIONAL BANK OF N.Y.',1,'Local','(786)777-6088/74',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(38,'SAFRA NATIONAL BANK OF N. Y.',1,'Local','777-6088',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(39,'OCEAN BANK',1,'Local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(40,'BANCO CONTINENTAL',1,'CALLE 50','11',0,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(41,'HSBC -PMA. (CTA. AHORRO)',1,'local','11',0,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(42,'PROVIDENT BK & TRUST  BELIZE',1,'local','11',0,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(43,'BCT BANK INTERNATIONAL',1,'local','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(44,'CITIBANK - PANAMA',1,'Local','11',0,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(45,'BANCO PANAME�O DE LA VIVIENDA',1,'S.A.','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(46,'BANCO PANAME�O DE LA  VIVIENDA',1,'S.A.','11',1,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(47,'BANVIVIENDA',1,'Local','11',0,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(48,'MULTIBANK (CTA CORRIENTE)',1,'Local','11',0,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(49,'MULTIBANK INVERSIONES',1,'Local','11',0,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(50,'MULTIBANK (CTA TRUST)',1,'Local','11',0,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(51,'BANCO UNIVERSAL',1,'Local','11',0,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(52,'MULTIBANK (DEPOSITO/OVERNIGHT)',1,'Local','11',0,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(53,'MULTIBANK PLAZO FIJO',1,'Local','11',0,'2025-07-17 22:35:00','2025-07-17 22:35:00'),(54,'MULTIBANK  CTA. AHORROS',1,'local','11',0,'2025-07-17 22:35:00','2025-07-17 22:35:00');
/*!40000 ALTER TABLE `bancos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(120) NOT NULL,
  `nrocliente` varchar(120) NOT NULL,
  `paise_id` int(11) NOT NULL,
  `direccion` text NOT NULL,
  `provincia_id` int(11) NOT NULL DEFAULT '1',
  `activo` int(11) DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `paise_id` (`paise_id`),
  KEY `clientes_ibfk_1` (`provincia_id`),
  CONSTRAINT `clientes_ibfk_1` FOREIGN KEY (`provincia_id`) REFERENCES `provincias` (`id`),
  CONSTRAINT `clientes_ibfk_2` FOREIGN KEY (`paise_id`) REFERENCES `paises` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'Pepsi Cola','4345566',1,'Esta es la dirección',1,1,'2017-07-24 15:48:04','2018-03-27 08:43:04'),(2,'Adidas','7688001216',1,'esta es la dirección',1,0,'2017-07-24 15:52:43','2018-04-09 15:03:54'),(3,'AT and T','123',1,'La dirección está aqui',2,1,'2017-07-24 15:53:52','2018-02-02 15:18:17'),(5,'GAP','',1,'Calle edif 000',2,1,'2017-07-24 17:39:58','2017-07-25 20:49:26'),(6,'Walmart','',1,'Calle 5 Edif 6',8,1,'2017-07-25 00:20:56','2017-07-25 22:31:13'),(9,'Pirelli','',1,'Esta es la dirección',5,1,'2017-07-25 17:39:35','2017-07-25 20:07:12'),(10,'Mazda Motor','12331166',1,'Calle 4 Local 6',7,1,'2017-07-25 20:11:54','2018-04-07 10:57:01'),(11,'Izusu','',1,'Calle 5 Ofic 6',8,1,'2017-07-25 22:21:23','2017-07-25 22:21:23'),(12,'Ford Motor ','',1,'Calle 5 ERdif 7',8,1,'2017-08-02 08:06:58','2017-08-02 08:06:58'),(13,'Kraft','7888121',1,'Calle 4 Edif. 8',8,1,'2017-08-02 08:35:29','2017-08-26 11:24:11'),(14,'Dominos Pizza','12133',1,'Calle 5 Edif 7',8,1,'2017-08-02 10:02:09','2018-04-09 15:04:20'),(15,'Starbucks','1211',1,'Calle50, No2',1,1,'2018-01-12 19:27:40','2018-04-09 14:59:30'),(16,'Starbucks','1211212',1,'Calle 50 N° 40',1,1,'2018-01-19 19:02:36','2018-01-19 19:02:36'),(17,'dede','12121',2,'dedede',1,1,'2018-01-19 19:51:08','2018-01-19 19:51:08'),(18,'dedede','4312121',2,'sasa as as as',1,1,'2018-01-20 14:02:54','2018-01-20 14:02:54'),(19,'Camejo Software','12111',4,'hyhyhy',1,1,'2018-01-20 14:03:58','2018-04-07 11:05:10'),(20,'Hola','2121212121',1,'Edif. 50 piso 39',1,1,'2018-01-28 15:24:40','2018-03-29 18:24:27');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes_personas`
--

DROP TABLE IF EXISTS `clientes_personas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes_personas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id` int(11) DEFAULT NULL,
  `persona_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes_personas`
--

LOCK TABLES `clientes_personas` WRITE;
/*!40000 ALTER TABLE `clientes_personas` DISABLE KEYS */;
INSERT INTO `clientes_personas` VALUES (1,17,1),(2,17,2),(3,17,4),(4,18,1),(5,18,2),(6,19,1),(7,19,4),(8,19,6),(9,19,8),(10,20,1),(11,20,6),(12,3,1),(13,1,1),(14,1,6),(15,10,2),(16,2,2),(17,15,8),(18,14,9);
/*!40000 ALTER TABLE `clientes_personas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `clientesdetallados`
--

DROP TABLE IF EXISTS `clientesdetallados`;
/*!50001 DROP VIEW IF EXISTS `clientesdetallados`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `clientesdetallados` AS SELECT 
 1 AS `id`,
 1 AS `cuenta_id`,
 1 AS `cliente_id`,
 1 AS `nombrecliente`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `clientesporcuentas`
--

DROP TABLE IF EXISTS `clientesporcuentas`;
/*!50001 DROP VIEW IF EXISTS `clientesporcuentas`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `clientesporcuentas` AS SELECT 
 1 AS `id`,
 1 AS `cuenta_id`,
 1 AS `cliente_id`,
 1 AS `nombre`,
 1 AS `activo`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `companias`
--

DROP TABLE IF EXISTS `companias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(120) NOT NULL,
  `descripcion` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companias`
--

LOCK TABLES `companias` WRITE;
/*!40000 ALTER TABLE `companias` DISABLE KEYS */;
INSERT INTO `companias` VALUES (1,'Uno Uno UNo','Esta es la descripción','2017-08-26 14:20:22','2017-08-26 14:20:22'),(2,'Dos Dos Dos','Esta es la descripción de la compañía','2017-08-26 14:38:44','2017-08-26 14:38:44');
/*!40000 ALTER TABLE `companias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuentas`
--

DROP TABLE IF EXISTS `cuentas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuentas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paise_id` int(11) NOT NULL,
  `banco_id` int(11) NOT NULL,
  `tipocuenta_id` int(11) NOT NULL,
  `moneda_id` int(11) NOT NULL,
  `compania_id` int(11) NOT NULL DEFAULT '1',
  `numero` varchar(120) NOT NULL,
  `saldo` decimal(10,4) NOT NULL,
  `tasa` decimal(10,4) NOT NULL,
  `vencimiento` date NOT NULL,
  `plazo` varchar(120) NOT NULL,
  `trust` tinyint(1) NOT NULL DEFAULT '1',
  `statucuenta_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tipocuenta_id` (`tipocuenta_id`),
  KEY `moneda_id` (`moneda_id`),
  KEY `statucuenta_id` (`statucuenta_id`),
  KEY `paise_id` (`paise_id`),
  KEY `banco_id` (`banco_id`),
  KEY `compania_id` (`compania_id`),
  CONSTRAINT `cuentas_ibfk_2` FOREIGN KEY (`tipocuenta_id`) REFERENCES `tipocuentas` (`id`),
  CONSTRAINT `cuentas_ibfk_4` FOREIGN KEY (`moneda_id`) REFERENCES `monedas` (`id`),
  CONSTRAINT `cuentas_ibfk_5` FOREIGN KEY (`statucuenta_id`) REFERENCES `statucuentas` (`id`),
  CONSTRAINT `cuentas_ibfk_6` FOREIGN KEY (`paise_id`) REFERENCES `paises` (`id`),
  CONSTRAINT `cuentas_ibfk_7` FOREIGN KEY (`banco_id`) REFERENCES `bancos` (`id`),
  CONSTRAINT `cuentas_ibfk_8` FOREIGN KEY (`compania_id`) REFERENCES `companias` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuentas`
--

LOCK TABLES `cuentas` WRITE;
/*!40000 ALTER TABLE `cuentas` DISABLE KEYS */;
INSERT INTO `cuentas` VALUES (1,1,1,1,1,1,'343455661111',4000.0000,5.0000,'2017-08-24','',1,1,'2017-08-24 18:20:11','2018-02-02 14:43:18'),(2,1,1,1,1,1,'12110099988',100.0000,10.0000,'2017-10-09','10',1,1,'2017-10-09 15:31:02','2018-02-02 14:43:02'),(3,1,1,2,1,2,'12221189000',100.0000,10.0000,'2017-10-12','10',1,1,'2017-10-12 18:01:46','2018-03-26 14:32:59'),(4,1,1,1,1,1,'1212121222331222000',100.5500,30.0000,'2018-02-02','10',1,1,'2018-02-02 14:31:28','2018-03-26 14:39:20'),(5,1,2,1,1,1,'120000800',200.0000,10.0000,'2018-02-03','10',1,1,'2018-02-03 12:34:07','2018-02-03 12:34:07'),(6,1,1,1,1,1,'12121333311122',1222.0000,12.7800,'2018-03-26','20',1,1,'2018-03-26 09:13:25','2018-03-26 14:32:11');
/*!40000 ALTER TABLE `cuentas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuentas_clientes`
--

DROP TABLE IF EXISTS `cuentas_clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuentas_clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cuenta_id` int(11) DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cuentas_clientes_1_idx` (`cuenta_id`),
  KEY `fk_cuentas_clientes_2_idx` (`cliente_id`),
  CONSTRAINT `fk_cuentas_clientes_1` FOREIGN KEY (`cuenta_id`) REFERENCES `cuentas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cuentas_clientes_2` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuentas_clientes`
--

LOCK TABLES `cuentas_clientes` WRITE;
/*!40000 ALTER TABLE `cuentas_clientes` DISABLE KEYS */;
INSERT INTO `cuentas_clientes` VALUES (4,4,6),(5,3,2),(7,2,19),(8,1,1),(9,1,9),(10,3,9),(11,3,10),(12,3,15),(13,5,9),(14,5,16),(15,6,1),(16,6,2);
/*!40000 ALTER TABLE `cuentas_clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monedas`
--

DROP TABLE IF EXISTS `monedas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monedas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moneda` varchar(120) NOT NULL,
  `cambio` decimal(10,2) NOT NULL DEFAULT '1.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monedas`
--

LOCK TABLES `monedas` WRITE;
/*!40000 ALTER TABLE `monedas` DISABLE KEYS */;
INSERT INTO `monedas` VALUES (1,'Dólar',1.20,'2017-07-26 08:24:26','2017-09-04 22:50:47'),(2,'Euro',2.10,'2017-07-26 08:25:07','2017-09-04 22:51:08'),(3,'Libra',1.34,'2017-07-26 08:25:19','2017-09-04 22:51:52'),(4,'Peso mexicano',1.00,'2018-01-12 19:36:40','2018-01-12 19:36:40');
/*!40000 ALTER TABLE `monedas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientos`
--

DROP TABLE IF EXISTS `movimientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimientos` (
  `id` char(36) NOT NULL,
  `operacione_id` int(11) NOT NULL,
  `cuenta_id` int(11) NOT NULL,
  `cliente_id` int(11) DEFAULT '1',
  `monto` decimal(10,4) NOT NULL,
  `created` datetime NOT NULL,
  `tipotransacciones_id` int(11) NOT NULL,
  `descripcion` varchar(120) DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cuenta_id` (`cuenta_id`),
  KEY `tipotransacciones_id` (`tipotransacciones_id`),
  KEY `operacione_id` (`operacione_id`),
  CONSTRAINT `movimientos_ibfk_2` FOREIGN KEY (`cuenta_id`) REFERENCES `cuentas` (`id`),
  CONSTRAINT `movimientos_ibfk_3` FOREIGN KEY (`tipotransacciones_id`) REFERENCES `tipotransacciones` (`id`),
  CONSTRAINT `movimientos_ibfk_5` FOREIGN KEY (`operacione_id`) REFERENCES `operaciones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientos`
--

LOCK TABLES `movimientos` WRITE;
/*!40000 ALTER TABLE `movimientos` DISABLE KEYS */;
INSERT INTO `movimientos` VALUES ('091f4ca0-bd90-48db-a808-f5824dc2ed2f',16,3,1,355.0000,'2017-11-21 00:00:00',1,NULL,'2017-12-20 15:14:55'),('0a68f134-fc4b-484a-ae57-ff17c8551399',16,2,1,200.0000,'2017-12-07 14:07:23',1,NULL,'2017-12-07 14:07:23'),('13f8434f-a577-4c14-9b92-4f0d0e140e17',18,1,1,3.0000,'2017-12-15 00:00:00',1,NULL,'2017-12-28 15:27:06'),('1434ba0e-3a12-45e9-b657-94162a5d6486',18,3,1,700.0000,'2017-12-20 13:56:02',2,NULL,'2017-12-20 13:56:02'),('14caa3eb-7372-4030-9233-3e017df3cc21',16,2,1,222.0000,'2018-01-12 00:00:00',2,NULL,'2018-01-12 19:56:46'),('16dd923d-fcd7-437c-b6c1-bb9ce8581de3',10,4,NULL,121212.0000,'2018-02-14 00:00:00',1,'esta es otra descripción','2018-02-14 11:20:37'),('29a74b6a-046b-44d3-82d7-e4ee4c984981',10,1,NULL,800.0000,'2018-02-14 00:00:00',2,'muy bien','2018-02-14 11:24:22'),('2c842cee-14e6-453a-929a-bd0aab483bb8',6,2,1,123.0000,'2017-11-01 08:17:11',2,NULL,'2017-11-01 08:17:11'),('2d2a5be2-92f4-4649-aac0-ad424b8dc6c8',3,2,1,1005.0000,'2017-11-01 15:19:37',2,NULL,'2017-11-01 15:19:37'),('316e1502-77f2-45f9-b145-cc6900e8383c',16,3,1,800.0000,'2017-12-19 00:00:00',2,NULL,'2017-12-20 15:13:13'),('3383f18c-2dc8-43be-ae88-b6a855dc6c9f',10,1,1,122.0000,'2017-11-20 14:39:47',1,'esta es una descripción','2018-03-29 17:55:56'),('34922c4a-6b17-450a-8da9-648885079750',16,2,1,1163.0000,'2017-12-15 00:00:00',1,NULL,'2017-12-28 10:22:07'),('36a5c7e1-3c6a-421d-920e-15c9f2e5121b',16,1,1,1162.0000,'2017-12-14 00:00:00',2,NULL,'2017-12-28 10:22:07'),('3db778e3-fca5-41d8-a364-ac5535310f76',16,4,4,600.0000,'2018-02-20 00:00:00',2,'este es nuevo','2018-02-19 12:06:01'),('3f5abec4-3857-4d4e-8774-e9caf483cb1b',10,3,2,6.0000,'2018-02-14 00:00:00',2,'bravoooooo','2018-02-14 11:23:53'),('415528f5-a4ce-40e2-961b-a244f4c4d638',12,1,1,290.0000,'2017-11-21 08:17:29',1,NULL,'2017-11-21 08:17:29'),('45763eb8-6e5f-4667-9c39-0756ef681516',17,1,1,400.0000,'2018-01-17 00:00:00',2,'todo normal','2018-01-17 13:08:28'),('46850a87-bae3-42e6-be88-a0023e49e630',16,2,1,1161.0000,'2017-12-15 00:00:00',2,NULL,'2017-12-28 10:22:07'),('4c124e08-0370-4a27-87ba-cb13a1f022f5',17,3,1,800.0000,'2017-12-15 00:00:00',1,NULL,'2017-12-28 10:19:24'),('4d03729c-2fa4-481d-89c4-e9a4bdacf124',18,1,1,800.0000,'2017-12-15 00:00:00',1,NULL,'2017-12-28 15:27:50'),('54be4c76-1aa4-460e-bd7d-ae49265c8b3d',18,3,1,1.0000,'2017-12-14 00:00:00',1,NULL,'2017-12-28 15:27:06'),('56619b59-98c7-4808-982c-4d5b749c5587',18,3,1,200.0000,'2018-02-21 00:00:00',2,'esta es la descripción, sí, es otra','2018-02-02 15:47:52'),('569af34e-9d86-489e-98ca-3c17b77d1fc2',6,2,1,121.0000,'2017-11-01 08:17:11',2,NULL,'2017-11-01 08:17:11'),('57434e58-1046-416d-b165-52ea3ed89968',17,3,1,10050.0000,'2017-12-20 13:53:19',1,NULL,'2017-12-20 13:53:19'),('57c4866a-4cb5-45a6-a99e-d70c0bc4529c',18,1,1,500.0000,'2017-12-26 00:00:00',1,NULL,'2017-12-28 15:31:26'),('57de4373-a681-4154-a7d3-dc6fe1bebd28',18,3,1,300.0000,'2018-01-28 00:00:00',2,'todo normal','2018-01-28 15:58:34'),('58ad4625-26b3-499a-af3a-b7274f1049df',17,3,1,856.7500,'2017-12-06 00:00:00',1,NULL,'2017-12-20 14:48:48'),('5aaad0af-ce85-4745-8a21-19153edd9bdb',8,2,1,100.0000,'2017-10-13 08:53:00',1,NULL,'2017-10-13 08:53:00'),('5c4b5c74-4e07-46f4-9fe1-d31a32d20098',18,4,1,600.0000,'2018-02-28 00:00:00',2,'fff','2018-02-02 15:53:44'),('66b1d4a6-920c-4476-8c9f-aee173fb9817',17,2,1,600.0000,'2017-12-20 13:51:56',1,NULL,'2017-12-20 13:51:56'),('7aa5fb44-a24c-494a-94fb-b76b849a3a62',8,2,1,122.0000,'2017-10-13 08:53:00',2,NULL,'2017-10-13 08:53:00'),('7d5f753c-a272-4b0a-918b-4c7a59bcf7e8',11,2,1,100.8900,'2017-11-20 14:40:35',2,NULL,'2017-11-20 14:40:35'),('80e3148a-18c6-4116-9e13-5a7a264e813f',7,1,1,100.0000,'2017-10-13 08:48:38',1,NULL,'2017-10-13 08:48:38'),('81d06b09-8897-4c3f-acc4-5f5a2b35f6a2',13,1,1,200.0000,'2017-11-23 12:53:54',2,NULL,'2017-11-23 12:53:54'),('8a0925e5-b749-4fc7-aeed-8eb1ae5a969d',17,3,1,700.0000,'2017-12-20 13:57:33',1,NULL,'2017-12-20 13:57:33'),('8cf98e1e-a305-41a7-894b-2e3b8e30fd2e',18,3,1,400.0000,'2018-02-12 00:00:00',2,'hooooohaaaa','2018-02-02 15:52:29'),('8fb316aa-294c-4b2a-b5ac-d968c4e1d58a',18,1,1,800.0000,'2017-12-14 00:00:00',2,NULL,'2017-12-28 15:27:49'),('94cf12a1-0a77-4287-973b-9072f3380b18',18,1,1,800.0000,'2017-12-15 00:00:00',1,NULL,'2017-12-28 15:27:49'),('95553e54-3814-40d2-8a68-cb46f3289cf9',17,2,1,700.0000,'2017-12-20 14:03:03',1,NULL,'2017-12-20 14:03:03'),('a2a23d0d-311c-4b05-88a6-2ce34f7be062',17,2,1,500.0000,'2017-12-15 00:00:00',1,NULL,'2017-12-28 10:19:24'),('a3f5a53d-b25d-46f2-a725-0e1fe47be064',10,4,5,200.0000,'2018-02-14 00:00:00',1,'este lleva la operación 10','2018-02-14 12:28:05'),('a794d1da-5472-402a-ae35-a16d5c8d9729',16,3,1,300.0000,'2018-01-19 00:00:00',2,'esta es otra operación','2018-01-19 15:05:44'),('a811919d-5ffd-412c-a9b5-76391c27f89f',15,2,1,100.0000,'2017-12-07 17:34:18',2,NULL,'2017-12-07 17:34:18'),('aaa56fed-6d49-4cac-a306-667090211b5a',3,2,1,45566.0000,'2017-11-01 15:19:37',1,NULL,'2017-11-01 15:19:37'),('b22a6e27-acef-45be-84c9-b22db8221175',10,3,6,200.0000,'2018-02-14 00:00:00',1,'3232','2018-02-14 11:19:52'),('c283198f-8f86-49f3-953d-278499a95d82',10,5,3,100.0000,'2018-02-14 00:00:00',2,'este tambien lleva la operación 10','2018-02-14 12:28:43'),('c7ab5da7-90de-4e20-ad0e-7ab058f84011',6,3,1,100.0000,'2017-11-01 15:01:09',2,NULL,'2017-11-01 15:01:09'),('c802a2ea-13f4-4967-abaa-f11243f2cf0a',19,2,1,200.0000,'2017-12-08 00:00:00',1,NULL,'2017-12-08 09:18:04'),('cad6e8df-fb8c-4da2-bef1-16c8f94b7e0a',16,2,1,300.0000,'2018-01-19 00:00:00',2,'hola','2018-01-19 15:14:27'),('cadfb39e-66eb-43cb-856a-a56d662396bf',18,1,1,2.0000,'2017-12-15 00:00:00',1,NULL,'2017-12-28 15:27:06'),('ce9b8f57-7661-4d8b-87eb-658cff790024',11,3,1,200.0000,'2017-11-20 14:40:35',2,NULL,'2017-11-20 14:40:35'),('cf8db5df-fdca-4a9b-afbc-6549d07eaff6',17,3,1,600.0000,'2017-12-26 00:00:00',1,NULL,'2017-12-20 14:46:51'),('d31c9668-4138-4659-849e-6ddcf144f2bc',18,3,1,400.0000,'2018-02-05 00:00:00',2,'hhyhy','2018-02-02 15:48:46'),('d4c4ee1a-4ce3-4b3b-a5fd-2b5e2b5c2461',10,1,11,600.0000,'2018-02-14 00:00:00',1,'ok ok ok ok ok','2018-02-14 12:29:32'),('da8a1dec-5053-44a7-8cad-27b3fd388ab4',10,4,12,200.0000,'2018-02-14 00:00:00',1,'hoooolaaaaa','2018-02-14 11:23:19'),('e1511125-d8c8-4a26-8d17-03bf242e1c38',18,3,1,560.9000,'2017-12-20 14:14:52',1,NULL,'2017-12-20 14:14:52'),('e4ff7a73-94b5-4089-9236-b84b6140a85d',16,3,1,500.0000,'2018-01-12 00:00:00',2,'esta es la descripción','2018-01-12 20:02:59'),('e5a9cac0-321f-4387-be31-69a98592e95c',18,2,1,500.0000,'2017-12-27 00:00:00',1,NULL,'2017-12-28 15:31:26'),('ef9d0dd0-e3ad-44ec-bc2b-332f8b7070f5',17,2,1,100.0000,'2017-12-07 17:33:45',1,NULL,'2017-12-07 17:33:45'),('f1852a27-bfd8-48a7-848f-e28c60739e2f',18,4,1,677.0000,'2018-02-13 00:00:00',2,'hooooola','2018-02-02 15:50:22'),('f63eb900-ca7b-4b2c-8670-0ca8b7ddba9a',17,1,1,300.0000,'2017-12-20 13:48:33',1,NULL,'2017-12-20 13:48:33'),('f65cbb01-cc0b-4bce-a629-73b28ea8146a',13,2,1,100.0000,'2017-11-23 12:53:54',1,NULL,'2017-11-23 12:53:54'),('f79a68b0-8a68-4861-a2de-cce28280a519',18,1,1,500.0000,'2017-12-04 00:00:00',1,NULL,'2017-12-28 15:31:26'),('ff068d6e-b3b1-4703-8d9f-2cd9a61ea277',17,1,1,100.0000,'2017-12-14 00:00:00',1,NULL,'2017-12-28 10:19:24');
/*!40000 ALTER TABLE `movimientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `new_view`
--

DROP TABLE IF EXISTS `new_view`;
/*!50001 DROP VIEW IF EXISTS `new_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `new_view` AS SELECT 
 1 AS `Clientes_CJoin__id`,
 1 AS `Clientes_CJoin__cuenta_id`,
 1 AS `Clientes_CJoin__cliente_id`,
 1 AS `Clientes__id`,
 1 AS `Clientes__nombre`,
 1 AS `Clientes__nrocliente`,
 1 AS `Clientes__paise_id`,
 1 AS `Clientes__direccion`,
 1 AS `Clientes__provincia_id`,
 1 AS `Clientes__created`,
 1 AS `Clientes__modified`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `operaciones`
--

DROP TABLE IF EXISTS `operaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(140) NOT NULL DEFAULT 'descripcion',
  `user_id` char(36) NOT NULL,
  `cerrado` tinyint(1) DEFAULT '0',
  `tipooperacione_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `fk_operaciones_1_idx` (`tipooperacione_id`),
  CONSTRAINT `fk_operaciones_1` FOREIGN KEY (`tipooperacione_id`) REFERENCES `tipooperaciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `operaciones_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operaciones`
--

LOCK TABLES `operaciones` WRITE;
/*!40000 ALTER TABLE `operaciones` DISABLE KEYS */;
INSERT INTO `operaciones` VALUES (3,'frfrfrf','d7262bae-cb51-4067-bf05-c01bcdf42e61',1,NULL,'2017-10-11 20:39:30','2017-11-01 15:25:52'),(6,'Esta es la descripción del Batch','d7262bae-cb51-4067-bf05-c01bcdf42e61',1,NULL,'2017-10-11 21:03:47','2017-11-01 15:01:09'),(7,'esta es una descripción','d7262bae-cb51-4067-bf05-c01bcdf42e61',0,NULL,'2017-10-13 08:48:38','2017-10-13 08:48:38'),(8,'pequeña descripción','d7262bae-cb51-4067-bf05-c01bcdf42e61',0,NULL,'2017-10-13 08:53:00','2017-10-13 08:53:00'),(10,'swsw','d7262bae-cb51-4067-bf05-c01bcdf42e61',0,1,'2017-11-20 14:39:47','2018-02-14 12:29:32'),(11,'dedede','d7262bae-cb51-4067-bf05-c01bcdf42e61',1,1,'2017-11-20 14:40:35','2017-11-30 13:41:02'),(12,'gtgtgt','d7262bae-cb51-4067-bf05-c01bcdf42e61',0,2,'2017-11-21 08:17:29','2017-11-21 08:17:29'),(13,'otro','d7262bae-cb51-4067-bf05-c01bcdf42e61',1,2,'2017-11-23 12:53:54','2017-11-23 12:59:05'),(14,'bacth','d7262bae-cb51-4067-bf05-c01bcdf42e61',0,2,'2017-12-07 13:32:41','2017-12-07 13:32:41'),(15,'bacth','d7262bae-cb51-4067-bf05-c01bcdf42e61',1,1,'2017-12-07 13:35:56','2017-12-08 09:15:06'),(16,'bacth333','d7262bae-cb51-4067-bf05-c01bcdf42e61',0,1,'2017-12-07 14:05:34','2018-02-19 12:06:01'),(17,'bacth','d7262bae-cb51-4067-bf05-c01bcdf42e61',0,1,'2017-12-07 14:07:59','2018-01-17 13:08:28'),(18,'bacth','d7262bae-cb51-4067-bf05-c01bcdf42e61',0,1,'2017-12-07 17:35:44','2018-02-02 15:53:44'),(19,'bacth','d7262bae-cb51-4067-bf05-c01bcdf42e61',1,1,'2017-12-08 09:13:35','2017-12-08 09:41:57'),(20,'bacth','d7262bae-cb51-4067-bf05-c01bcdf42e61',0,1,'2018-02-19 12:06:25','2018-02-19 12:06:25');
/*!40000 ALTER TABLE `operaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paises`
--

DROP TABLE IF EXISTS `paises`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pais` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paises`
--

LOCK TABLES `paises` WRITE;
/*!40000 ALTER TABLE `paises` DISABLE KEYS */;
INSERT INTO `paises` VALUES (1,'Panamá'),(2,'Estados Unidos'),(3,'España'),(4,'Venezuela');
/*!40000 ALTER TABLE `paises` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personas`
--

DROP TABLE IF EXISTS `personas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(120) NOT NULL,
  `cedula` int(120) NOT NULL DEFAULT '111',
  `email` varchar(120) NOT NULL DEFAULT 'correo@correo.com',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personas`
--

LOCK TABLES `personas` WRITE;
/*!40000 ALTER TABLE `personas` DISABLE KEYS */;
INSERT INTO `personas` VALUES (1,'Jose Rafael Camejo',11683419,'webmaster@joserafael.com.ve','2017-07-27 00:00:00','2017-07-27 00:00:00'),(2,'Juan Pérez',1212121,'juanperez@gmail.com','2017-07-25 18:00:06','2017-07-25 18:00:06'),(4,'Pedro García',212121,'pedrogarcia@gmail.com','2017-07-25 20:11:54','2017-07-25 20:11:54'),(5,'María Pérez',767676,'mariadede@gmail.com','2017-07-25 20:38:55','2017-07-25 20:38:55'),(6,'Juan garcía',212133311,'juanggg@gmail.com','2017-07-25 20:49:26','2017-07-25 20:49:26'),(8,'Alberto García',212122334,'albertogtgt@gmail.com','2017-07-25 21:25:43','2017-10-04 09:28:29'),(9,'Luisa Gutierrez',21212121,'luisa56565@gmail.com','2017-07-25 22:21:23','2017-08-02 07:46:48'),(10,'Felix Pérez',212122222,'feliz222@gmail.com','2017-07-25 22:31:13','2017-07-25 22:31:13'),(11,'Carmen garcía',21411786,'carmengarcia@gmail.com','2017-08-02 07:46:00','2017-08-02 07:46:36'),(12,'Ana González',111,'correo@correo.com','2018-01-12 17:05:29','2018-01-12 17:05:29');
/*!40000 ALTER TABLE `personas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personas_telefonos`
--

DROP TABLE IF EXISTS `personas_telefonos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personas_telefonos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `persona_id` int(11) NOT NULL,
  `telefono_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `persona_id` (`persona_id`),
  KEY `telefono_id` (`telefono_id`),
  CONSTRAINT `personas_telefonos_ibfk_1` FOREIGN KEY (`persona_id`) REFERENCES `personas` (`id`),
  CONSTRAINT `personas_telefonos_ibfk_2` FOREIGN KEY (`telefono_id`) REFERENCES `telefonos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personas_telefonos`
--

LOCK TABLES `personas_telefonos` WRITE;
/*!40000 ALTER TABLE `personas_telefonos` DISABLE KEYS */;
/*!40000 ALTER TABLE `personas_telefonos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provincias`
--

DROP TABLE IF EXISTS `provincias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provincias` (
  `id` int(11) NOT NULL,
  `paise_id` int(11) DEFAULT NULL,
  `provincia` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provincias`
--

LOCK TABLES `provincias` WRITE;
/*!40000 ALTER TABLE `provincias` DISABLE KEYS */;
INSERT INTO `provincias` VALUES (1,1,'Bocas del Toro'),(2,1,'Coclé'),(3,1,'Colón'),(4,1,'Chiriquí'),(5,1,'Darién'),(6,1,'Herrera'),(7,1,'Los Santos'),(8,1,'Panamá'),(9,1,'Veraguas'),(10,1,'Panamá Oeste'),(11,2,'California');
/*!40000 ALTER TABLE `provincias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statucuentas`
--

DROP TABLE IF EXISTS `statucuentas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statucuentas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statucuentas`
--

LOCK TABLES `statucuentas` WRITE;
/*!40000 ALTER TABLE `statucuentas` DISABLE KEYS */;
INSERT INTO `statucuentas` VALUES (1,'Activo'),(2,'Inactivo');
/*!40000 ALTER TABLE `statucuentas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telefonos`
--

DROP TABLE IF EXISTS `telefonos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefonos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id` int(11) NOT NULL DEFAULT '1',
  `persona_id` int(11) NOT NULL DEFAULT '1',
  `telefono` varchar(120) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cliente_id` (`cliente_id`),
  KEY `persona_id` (`persona_id`),
  CONSTRAINT `telefonos_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`),
  CONSTRAINT `telefonos_ibfk_2` FOREIGN KEY (`persona_id`) REFERENCES `personas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telefonos`
--

LOCK TABLES `telefonos` WRITE;
/*!40000 ALTER TABLE `telefonos` DISABLE KEYS */;
INSERT INTO `telefonos` VALUES (3,1,1,'54545454','2017-07-25 15:34:53','2017-07-25 15:34:53'),(5,6,1,'2121333','2017-07-25 15:36:24','2017-07-25 15:36:24'),(6,6,1,'121121','2017-07-25 15:36:24','2017-07-25 15:36:24'),(8,3,1,'12121212','2017-07-25 16:33:28','2017-07-25 16:33:28'),(9,9,1,'12121212','2017-07-25 19:56:35','2017-07-25 19:56:35'),(10,10,1,'212121','2017-07-25 20:11:54','2017-07-25 20:11:54'),(11,10,1,'21212333','2017-07-25 20:11:54','2017-07-25 20:11:54'),(12,5,1,'8779012','2017-07-25 20:38:55','2017-07-25 20:38:55'),(13,11,1,'43434343','2017-07-25 22:21:23','2017-07-25 22:21:23'),(14,11,1,'43434343','2017-07-25 22:21:23','2017-07-25 22:21:23'),(16,12,1,'3232344','2017-08-02 08:06:58','2017-08-02 08:06:58'),(17,12,1,'4545454','2017-08-02 08:06:58','2017-08-02 08:06:58'),(18,13,1,'656565','2017-08-02 08:35:29','2017-08-02 08:35:29'),(19,13,1,'656565','2017-08-02 08:35:29','2017-08-02 08:35:29'),(20,14,1,'344343434','2017-08-02 10:02:09','2017-08-02 10:02:09'),(21,14,1,'4343543','2017-08-02 10:02:09','2017-08-02 10:02:09'),(22,15,1,'4324456','2018-01-12 19:27:40','2018-01-12 19:27:40'),(23,15,1,'1233111','2018-01-12 19:27:40','2018-01-12 19:27:40'),(24,16,1,'232323','2018-01-19 19:02:36','2018-01-19 19:02:36'),(25,16,1,'432446','2018-01-19 19:02:36','2018-01-19 19:02:36'),(26,17,1,'545454','2018-01-19 19:51:08','2018-01-19 19:51:08'),(27,17,1,'545454555333','2018-01-19 19:51:08','2018-01-19 19:51:08'),(28,18,1,'657675766','2018-01-20 14:02:54','2018-01-20 14:02:54'),(29,18,1,'4545454','2018-01-20 14:02:54','2018-01-20 14:02:54'),(30,18,1,'454545545','2018-01-20 14:02:54','2018-01-20 14:02:54'),(31,19,1,'34343','2018-01-20 14:03:58','2018-01-20 14:03:58'),(32,20,1,'2121213333','2018-01-28 15:24:40','2018-01-28 15:24:40'),(33,20,1,'55661122','2018-01-28 15:24:40','2018-01-28 15:24:40'),(34,1,1,'11223311','2018-02-22 06:51:20','2018-02-22 06:51:20'),(35,2,1,'121122367','2018-04-09 15:03:54','2018-04-09 15:03:54');
/*!40000 ALTER TABLE `telefonos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipocuentas`
--

DROP TABLE IF EXISTS `tipocuentas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipocuentas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipocuenta` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipocuentas`
--

LOCK TABLES `tipocuentas` WRITE;
/*!40000 ALTER TABLE `tipocuentas` DISABLE KEYS */;
INSERT INTO `tipocuentas` VALUES (1,'Ahorros'),(2,'Corriente'),(3,'Plazo Fijo');
/*!40000 ALTER TABLE `tipocuentas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipooperaciones`
--

DROP TABLE IF EXISTS `tipooperaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipooperaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipooperaciones`
--

LOCK TABLES `tipooperaciones` WRITE;
/*!40000 ALTER TABLE `tipooperaciones` DISABLE KEYS */;
INSERT INTO `tipooperaciones` VALUES (1,'Batch'),(2,'Cierre Semanal');
/*!40000 ALTER TABLE `tipooperaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipotransacciones`
--

DROP TABLE IF EXISTS `tipotransacciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipotransacciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaccion` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipotransacciones`
--

LOCK TABLES `tipotransacciones` WRITE;
/*!40000 ALTER TABLE `tipotransacciones` DISABLE KEYS */;
INSERT INTO `tipotransacciones` VALUES (1,'Crédito'),(2,'Débito');
/*!40000 ALTER TABLE `tipotransacciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `activo` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('32bffcdb-f408-43b8-b891-9427cb3d56b3','operador','$2y$10$PsjTJT4/VlfliZs.p/5./.3OiUH8YHRNkNAYbVz5BLmZV4Mlo3SVe','operador',NULL,'2018-05-23 06:25:37','2018-05-23 07:00:44'),('90660957-fde1-4d6e-8ce8-fc9cd7755ea0','consultor','$2y$10$.Y5e26Uz8DhgEwh.DKBJ.OhfJG5oaw2SlIvaoNW1gonGq1k5T1F9y','consultor',NULL,'2018-05-23 07:00:20','2018-05-23 07:00:20'),('d7262bae-cb51-4067-bf05-c01bcdf42e61','admin','$2y$10$oDvrrJCasLbl9LfsKUSpJu6FoVMDUBkX/l0STI3lcue1g7kV7jydC','admin',NULL,'2017-06-06 15:58:26','2017-06-06 15:58:26');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `clientesdetallados`
--

/*!50001 DROP VIEW IF EXISTS `clientesdetallados`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `clientesdetallados` AS select `CuentasClientes`.`id` AS `id`,`CuentasClientes`.`cuenta_id` AS `cuenta_id`,`CuentasClientes`.`cliente_id` AS `cliente_id`,`Clientes`.`nombre` AS `nombrecliente` from (`clientes` `Clientes` join `cuentas_clientes` `CuentasClientes` on((`Clientes`.`id` = `CuentasClientes`.`cliente_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `clientesporcuentas`
--

/*!50001 DROP VIEW IF EXISTS `clientesporcuentas`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `clientesporcuentas` AS select `CuentasClientes`.`id` AS `id`,`CuentasClientes`.`cuenta_id` AS `cuenta_id`,`Clientes`.`id` AS `cliente_id`,`Clientes`.`nombre` AS `nombre`,`Clientes`.`activo` AS `activo` from (`clientes` `Clientes` join `cuentas_clientes` `CuentasClientes` on((`Clientes`.`id` = `CuentasClientes`.`cliente_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `new_view`
--

/*!50001 DROP VIEW IF EXISTS `new_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `new_view` AS select `CuentasClientes`.`id` AS `Clientes_CJoin__id`,`CuentasClientes`.`cuenta_id` AS `Clientes_CJoin__cuenta_id`,`CuentasClientes`.`cliente_id` AS `Clientes_CJoin__cliente_id`,`Clientes`.`id` AS `Clientes__id`,`Clientes`.`nombre` AS `Clientes__nombre`,`Clientes`.`nrocliente` AS `Clientes__nrocliente`,`Clientes`.`paise_id` AS `Clientes__paise_id`,`Clientes`.`direccion` AS `Clientes__direccion`,`Clientes`.`provincia_id` AS `Clientes__provincia_id`,`Clientes`.`created` AS `Clientes__created`,`Clientes`.`modified` AS `Clientes__modified` from (`clientes` `Clientes` join `cuentas_clientes` `CuentasClientes` on((`Clientes`.`id` = `CuentasClientes`.`cliente_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-24  9:17:17
