SELECT
        `cuentas`.`id` AS `id`,
        `cuentas`.`numero` AS `numero`,
        `clientes`.`nombre` AS `cliente`,
        `bancos`.`nombre` AS `banco`,
        `monedas`.`moneda` AS `moneda`

    FROM
        (`cuentas`
        LEFT JOIN `clientes` ON ((`clientes`.`id` = `cuentas`.`cliente_id`))
        LEFT JOIN `bancos` ON ((`bancos`.`id` = `cuentas`.`banco_id`))
        LEFT JOIN `monedas` ON ((`monedas`.`id` = `cuentas`.`moneda_id`))

      
      )
    WHERE
        (`cuentas`.`id` IS NOT NULL)
    ORDER BY `cuentas`.`id`




ALTER TABLE `fiduciaria`.`clientes` 
DROP FOREIGN KEY `clientes_ibfk_3`;
ALTER TABLE `fiduciaria`.`clientes` 
DROP COLUMN `persona_id`,
DROP INDEX `persona_id` ;

