ALTER TABLE `fiduciaria`.`cuentas` 
CHANGE COLUMN `tasa` `tasa` DECIMAL(10,4) NOT NULL ;


ALTER TABLE `fiduciaria`.`cuentas` 
CHANGE COLUMN `saldo` `saldo` DECIMAL(10,4) NOT NULL ;
