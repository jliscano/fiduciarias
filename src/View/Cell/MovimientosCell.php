<?php
namespace App\View\Cell;

use Cake\View\Cell;

/**
 * Movimientos cell
 */
class MovimientosCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {

        
      
        $this->loadModel('Tipotransacciones');
        $this->loadModel('Cuentas');
       
        $tipotransacciones = $this->Tipotransacciones->find('list');
        $cuentas = $this->Cuentas->find('list');
        $this->set(compact('tipotransacciones', 'cuentas'));



    }
}
