<?php
namespace App\Controller;

use App\Controller\AppController;


class DetallecuentasController extends AppController
{

    
    public function index()
    {
        $this->viewBuilder()->layout(false);
        
        $detallecuentas = $this->Detallecuentas
        ->find('all');
        
        $this->set(
            ['detallecuentas' => $detallecuentas,
            '_serialize' => 'detallecuentas']);
    }

}



