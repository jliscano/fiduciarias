<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Tipocuentas Controller
 *
 * @property \App\Model\Table\TipocuentasTable $Tipocuentas
 *
 * @method \App\Model\Entity\Tipocuenta[] paginate($object = null, array $settings = [])
 */
class TipocuentasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

      $this->paginate = [

          'limit' => 2000
      ];
        $tipocuentas = $this->paginate($this->Tipocuentas);

        $this->set(compact('tipocuentas'));
        $this->set('_serialize', ['tipocuentas']);
    }

    /**
     * View method
     *
     * @param string|null $id Tipocuenta id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tipocuenta = $this->Tipocuentas->get($id, [
            'contain' => ['Cuentas' =>['Clientes', 'Monedas']]
        ]);

        $this->set('tipocuenta', $tipocuenta);
        $this->set('_serialize', ['tipocuenta']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tipocuenta = $this->Tipocuentas->newEntity();
        if ($this->request->is('post')) {
            $tipocuenta = $this->Tipocuentas->patchEntity($tipocuenta, $this->request->getData());
            if ($this->Tipocuentas->save($tipocuenta)) {
                $this->Flash->success(__('La información fue guardada.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo guardar la información.'));
        }
        $this->set(compact('tipocuenta'));
        $this->set('_serialize', ['tipocuenta']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipocuenta id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tipocuenta = $this->Tipocuentas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipocuenta = $this->Tipocuentas->patchEntity($tipocuenta, $this->request->getData());
            if ($this->Tipocuentas->save($tipocuenta)) {
                $this->Flash->success(__('La información fue editada.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo editar la información.'));
        }
        $this->set(compact('tipocuenta'));
        $this->set('_serialize', ['tipocuenta']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipocuenta id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tipocuenta = $this->Tipocuentas->get($id);
        if ($this->Tipocuentas->delete($tipocuenta)) {
            $this->Flash->success(__('La información fue borrada.'));
        } else {
            $this->Flash->error(__('No se pudo borrar la información.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
