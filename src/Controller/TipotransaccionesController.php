<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Tipotransacciones Controller
 *
 * @property \App\Model\Table\TipotransaccionesTable $Tipotransacciones
 *
 * @method \App\Model\Entity\Tipotransaccione[] paginate($object = null, array $settings = [])
 */
class TipotransaccionesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $tipotransacciones = $this->paginate($this->Tipotransacciones);

        $this->set(compact('tipotransacciones'));
        $this->set('_serialize', ['tipotransacciones']);
    }

    /**
     * View method
     *
     * @param string|null $id Tipotransaccione id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tipotransaccione = $this->Tipotransacciones->get($id, [
            'contain' => []
        ]);

        $this->set('tipotransaccione', $tipotransaccione);
        $this->set('_serialize', ['tipotransaccione']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tipotransaccione = $this->Tipotransacciones->newEntity();
        if ($this->request->is('post')) {
            $tipotransaccione = $this->Tipotransacciones->patchEntity($tipotransaccione, $this->request->getData());
            if ($this->Tipotransacciones->save($tipotransaccione)) {
                $this->Flash->success(__('The tipotransaccione has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tipotransaccione could not be saved. Please, try again.'));
        }
        $this->set(compact('tipotransaccione'));
        $this->set('_serialize', ['tipotransaccione']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipotransaccione id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tipotransaccione = $this->Tipotransacciones->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipotransaccione = $this->Tipotransacciones->patchEntity($tipotransaccione, $this->request->getData());
            if ($this->Tipotransacciones->save($tipotransaccione)) {
                $this->Flash->success(__('The tipotransaccione has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tipotransaccione could not be saved. Please, try again.'));
        }
        $this->set(compact('tipotransaccione'));
        $this->set('_serialize', ['tipotransaccione']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipotransaccione id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tipotransaccione = $this->Tipotransacciones->get($id);
        if ($this->Tipotransacciones->delete($tipotransaccione)) {
            $this->Flash->success(__('The tipotransaccione has been deleted.'));
        } else {
            $this->Flash->error(__('The tipotransaccione could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
