<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Telefonos Controller
 *
 * @property \App\Model\Table\TelefonosTable $Telefonos
 *
 * @method \App\Model\Entity\Telefono[] paginate($object = null, array $settings = [])
 */
class TelefonosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

         $this->paginate = [
            'contain' => ['Clientes'],
            'limit' => 20000
        ];
        $telefonos = $this->paginate($this->Telefonos);

        $this->set(compact('telefonos'));
        $this->set('_serialize', ['telefonos']);
    }

    /**
     * View method
     *
     * @param string|null $id Telefono id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $telefono = $this->Telefonos->get($id, [
            'contain' => ['Clientes', 'Personas']
        ]);

        $this->set('telefono', $telefono);
        $this->set('_serialize', ['telefono']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $telefono = $this->Telefonos->newEntity();
        if ($this->request->is('post')) {
            $telefono = $this->Telefonos->patchEntity($telefono, $this->request->getData());
            if ($this->Telefonos->save($telefono)) {
                $this->Flash->success(__('The telefono has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The telefono could not be saved. Please, try again.'));
        }
        $clientes = $this->Telefonos->Clientes->find('list', ['limit' => 200]);
        $personas = $this->Telefonos->Personas->find('list', ['limit' => 200]);
        $this->set(compact('telefono', 'clientes', 'personas'));
        $this->set('_serialize', ['telefono']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Telefono id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $telefono = $this->Telefonos->get($id, [
            'contain' => ['Clientes', 'Personas']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $telefono = $this->Telefonos->patchEntity($telefono, $this->request->getData());
            if ($this->Telefonos->save($telefono)) {
                $this->Flash->success(__('The telefono has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The telefono could not be saved. Please, try again.'));
        }
        $clientes = $this->Telefonos->Clientes->find('list', ['limit' => 200]);
        $personas = $this->Telefonos->Personas->find('list', ['limit' => 200]);
        $this->set(compact('telefono', 'clientes', 'personas'));
        $this->set('_serialize', ['telefono']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Telefono id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $telefono = $this->Telefonos->get($id);
        if ($this->Telefonos->delete($telefono)) {
            $this->Flash->success(__('El número telefónico ha sido borrado.'));
        } else {
            $this->Flash->error(__('The telefono could not be deleted. Please, try again.'));
        }

       return $this->redirect($this->referer());
    }
}
