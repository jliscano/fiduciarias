<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Bancos Controller
 *
 * @property \App\Model\Table\BancosTable $Bancos
 *
 * @method \App\Model\Entity\Banco[] paginate($object = null, array $settings = [])
 */
class BancosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        $this->paginate = [
            'contain' => ['Paises'],
            'limit' => 2000
        ];
        $bancos = $this->paginate($this->Bancos);
        $activado = ['1' => 'Sí', '0' => 'No'];
        $this->set(compact('bancos', 'activado'));
        $this->set('_serialize', ['bancos']);
    }

    /**
     * View method
     *
     * @param string|null $id Banco id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $banco = $this->Bancos->get($id, [
            'contain' => ['Paises']
        ]);

        $this->set('banco', $banco);
        $this->set('_serialize', ['banco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $banco = $this->Bancos->newEntity();
        if ($this->request->is('post')) {
            $banco = $this->Bancos->patchEntity($banco, $this->request->getData());
            if ($this->Bancos->save($banco)) {
                $this->Flash->success(__('La información ha sideo guardada.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo guardar la información. Inténtelo nuevamente.'));
        }
         $paises = $this->Bancos->Paises->find('list', ['limit' => 200]);
        $this->set(compact('banco', 'paises'));
        $this->set('_serialize', ['banco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Banco id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $banco = $this->Bancos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $banco = $this->Bancos->patchEntity($banco, $this->request->getData());
            if ($this->Bancos->save($banco)) {
                $this->Flash->success(__('La información ha sido editada'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo editarla información. Inténtelo nuevamente'));
        }
         $paises = $this->Bancos->Paises->find('list', ['limit' => 200]);
        $this->set(compact('banco', 'paises'));
        $this->set('_serialize', ['banco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Banco id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $banco = $this->Bancos->get($id);
        if ($this->Bancos->delete($banco)) {
            $this->Flash->success(__('La información ha sido borrada.'));
        } else {
            $this->Flash->error(__('No se pudo borrar la inforamción. Inténtelo nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
