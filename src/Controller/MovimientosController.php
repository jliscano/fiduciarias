<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Number;
use Cake\I18n\Time;
/**
 * Movimientos Controller
 *
 * @property \App\Model\Table\MovimientosTable $Movimientos
 *
 * @method \App\Model\Entity\Movimiento[] paginate($object = null, array $settings = [])
 */
class MovimientosController extends AppController
{


  public function initialize()
  {
      parent::initialize();

      $this->loadComponent('RequestHandler');

      }



    public function index()
    {
        $this->paginate = [
            'contain' => ['Cuentas' => ['Bancos'], 'Tipotransacciones', 'Operaciones'],
            'limit' => 2000
        ];
         $movimientos = $this->paginate($this->Movimientos);

        $this->set(compact('movimientos'));
        $this->set('_serialize', ['movimientos']);
    }

    /**
     * View method
     *
     * @param string|null $id Movimiento id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $movimiento = $this->Movimientos->get($id, [
            'contain' => ['Cuentas', 'Tipotransacciones', 'Operaciones']
        ]);

        $this->set('movimiento', $movimiento);
        $this->set('_serialize', ['movimiento']);

    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {


        $movimiento = $this->Movimientos->newEntity();
        if ($this->request->is('post')) {
          //  $movimiento->user_id = $this->Auth->user('id');
            $movimiento = $this->Movimientos->patchEntities($movimiento, $this->request->getData());
            if ($this->Movimientos->save($movimiento)) {
                $this->Flash->success(__('La información ha sido guardada.'));

                return $this->redirect($this->referer());
                //$this->redirect(['controller' => 'Operaciones', 'action' => 'view', $this->request->$movimientos->id]);
            }
            $this->Flash->error(__('The movimiento could not be saved. Please, try again.'));
        }
        $clientes = $this->Movimientos->Clientes->find('list', ['limit' => 200]);
        $cuentas = $this->Movimientos->Cuentas->find('list', ['limit' => 200]);
        $tipotransacciones = $this->Movimientos->Tipotransacciones->find('list', ['limit' => 200]);

        $this->set(compact('movimiento', 'clientes', 'cuentas', 'tipotransacciones'));
        $this->set('_serialize', ['movimiento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Movimiento id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $movimiento = $this->Movimientos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $movimiento = $this->Movimientos->patchEntity($movimiento, $this->request->getData());
            if ($this->Movimientos->save($movimiento)) {
                $this->Flash->success(__('El movimiento ha sido editado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo editar la información. Revise el error e intente nuevamente.'));
        }
        $clientes = $this->Movimientos->Clientes->find('list', ['limit' => 200]);
        $cuentas = $this->Movimientos->Cuentas->find('list', ['limit' => 200]);
        $tipotransacciones = $this->Movimientos->Tipotransacciones->find('list', ['limit' => 200]);
        $users = $this->Movimientos->Users->find('list', ['limit' => 200]);
        $this->set(compact('movimiento', 'clientes', 'cuentas', 'tipotransacciones', 'users'));
        $this->set('_serialize', ['movimiento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Movimiento id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $movimiento = $this->Movimientos->get($id);
        if ($this->Movimientos->delete($movimiento)) {
            $this->Flash->success(__('The movimiento has been deleted.'));
        } else {
            $this->Flash->error(__('The movimiento could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function reportes() {

      {
          $this->paginate = [
              'contain' => ['Cuentas' => ['Bancos', 'Monedas'], 'Tipotransacciones', 'Operaciones', 'Clientes'],
              'limit' => 2000
          ];

           $movimientos = $this->paginate($this->Movimientos);

          $this->set(compact('movimientos'));
          $this->set('_serialize', ['movimientos']);
      }




    }

    public function cuentas()

        {


        $this->viewBuilder()->layout('ajax');
        $clientes = $this->request->data['cliente_id'];

        $cuentas = $this->Movimientos->Cuentas->find('list', ['limit' => 2000, 'conditions' => ['Cuentas.cliente_id' => $clientes]]);

           $this->set(compact('cuentas'));
           $this->set('_serialize', ['movimiento']);
        }
}
