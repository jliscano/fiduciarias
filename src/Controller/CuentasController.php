<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Cuentas Controller
 *
 * @property \App\Model\Table\CuentasTable $Cuentas
 *
 * @method \App\Model\Entity\Cuenta[] paginate($object = null, array $settings = [])
 */
class CuentasController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Clientes', 'Tipocuentas', 'Monedas', 'Statucuentas', 'Bancos', 'Movimientos' => ['Tipotransacciones']]
        ];


        $qurysaldo = $this->Cuentas->Movimientos->find();



        $query = $this->Cuentas->find('all', [
            'contain' => ['Clientes', 'Tipocuentas', 'Monedas', 'Statucuentas', 'Bancos', 'Movimientos' => ['Tipotransacciones']]
        ]);


        $cuentas = $this->paginate($query);

        $this->set(compact('cuentas'));
        $this->set('_serialize', ['cuentas']);
    }

    /**
     * View method
     *
     * @param string|null $id Cuenta id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $cuenta = $this->Cuentas->get($id, [
            'contain' => ['Clientes', 'Tipocuentas', 'Monedas', 'Statucuentas', 'Paises', 'Bancos', 'Companias', 'Movimientos' => ['Tipotransacciones']]
        ]);


        $transaccion = [1 => 'Crédito', 2 => 'Débito'];
        $this->set(compact('transaccion'));
        $this->set('cuenta', $cuenta);

        $this->set('_serialize', ['cuenta']);
    }

    public function simple() {

        $this->loadModel('Detallecuentas');
        $this->viewBuilder()->layout(false);

        $detallecuentas = $this->Detallecuentas
                ->find('all');

        //->select(['id', 'numero']);





        $this->set(
                ['detallecuentas' => $detallecuentas,
                    '_serialize' => 'detallecuentas']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $cuenta = $this->Cuentas->newEntity();
        if ($this->request->is('post')) {
            $cuenta = $this->Cuentas->patchEntity($cuenta, $this->request->getData());
            if ($this->Cuentas->save($cuenta)) {
                $this->Flash->success(__('La información ha sido guardada.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo guardarla información. Inténtelo nuevamente.'));
        }
        $clientes = $this->Cuentas->Clientes->find('list', ['limit' => 200]);
        $tipocuentas = $this->Cuentas->Tipocuentas->find('list', ['limit' => 200]);
        $monedas = $this->Cuentas->Monedas->find('list', ['limit' => 200]);
        $statucuentas = $this->Cuentas->Statucuentas->find('list', ['limit' => 200]);
        $paises = $this->Cuentas->Paises->find('list', ['limit' => 200]);
        $bancos = $this->Cuentas->Bancos->find('list', ['limit' => 200]);
        $companias = $this->Cuentas->Companias->find('list', ['limit' => 200]);
        $this->set(compact('cuenta', 'clientes', 'tipocuentas', 'monedas', 'statucuentas', 'paises', 'bancos', 'companias'));
        $this->set('_serialize', ['cuenta']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cuenta id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $cuenta = $this->Cuentas->get($id, [
            'contain' => ['Clientes']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cuenta = $this->Cuentas->patchEntity($cuenta, $this->request->getData());
            if ($this->Cuentas->save($cuenta)) {
                $this->Flash->success(__('La información ha sido editada.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo editar la información. Inténtelo nuevamente.'));
        }
        $clientes = $this->Cuentas->Clientes->find('list', ['limit' => 200]);
        $tipocuentas = $this->Cuentas->Tipocuentas->find('list', ['limit' => 200]);
        $monedas = $this->Cuentas->Monedas->find('list', ['limit' => 200]);
        $statucuentas = $this->Cuentas->Statucuentas->find('list', ['limit' => 200]);
        $paises = $this->Cuentas->Paises->find('list', ['limit' => 200]);
        $bancos = $this->Cuentas->Bancos->find('list', ['limit' => 200]);
        $companias = $this->Cuentas->Companias->find('list', ['limit' => 200]);
        $this->set(compact('cuenta', 'clientes', 'tipocuentas', 'monedas', 'statucuentas', 'paises', 'bancos', 'companias'));

        $this->set('_serialize', ['cuenta']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cuenta id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $cuenta = $this->Cuentas->get($id);
        if ($this->Cuentas->delete($cuenta)) {
            $this->Flash->success(__('La información ha sido borrada.'));
        } else {
            $this->Flash->error(__('No se pudo borrar la información. Inténtelo nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
