<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Statucuentas Controller
 *
 * @property \App\Model\Table\StatucuentasTable $Statucuentas
 *
 * @method \App\Model\Entity\Statucuenta[] paginate($object = null, array $settings = [])
 */
class StatucuentasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $statucuentas = $this->paginate($this->Statucuentas);

        $this->set(compact('statucuentas'));
        $this->set('_serialize', ['statucuentas']);
    }

    /**
     * View method
     *
     * @param string|null $id Statucuenta id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $statucuenta = $this->Statucuentas->get($id, [
            'contain' => ['Cuentas']
        ]);

        $this->set('statucuenta', $statucuenta);
        $this->set('_serialize', ['statucuenta']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $statucuenta = $this->Statucuentas->newEntity();
        if ($this->request->is('post')) {
            $statucuenta = $this->Statucuentas->patchEntity($statucuenta, $this->request->getData());
            if ($this->Statucuentas->save($statucuenta)) {
                $this->Flash->success(__('The statucuenta has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The statucuenta could not be saved. Please, try again.'));
        }
        $this->set(compact('statucuenta'));
        $this->set('_serialize', ['statucuenta']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Statucuenta id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $statucuenta = $this->Statucuentas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $statucuenta = $this->Statucuentas->patchEntity($statucuenta, $this->request->getData());
            if ($this->Statucuentas->save($statucuenta)) {
                $this->Flash->success(__('The statucuenta has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The statucuenta could not be saved. Please, try again.'));
        }
        $this->set(compact('statucuenta'));
        $this->set('_serialize', ['statucuenta']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Statucuenta id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $statucuenta = $this->Statucuentas->get($id);
        if ($this->Statucuentas->delete($statucuenta)) {
            $this->Flash->success(__('The statucuenta has been deleted.'));
        } else {
            $this->Flash->error(__('The statucuenta could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
