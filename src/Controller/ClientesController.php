<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\CuentasCliente;
use Cake\ORM\TableRegistry;

/**
 * Clientes Controller
 *
 * @property \App\Model\Table\ClientesTable $Clientes
 *
 * @method \App\Model\Entity\Cliente[] paginate($object = null, array $settings = [])
 */
class ClientesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Provincias', 'Paises', 'Telefonos', 'Personas', 'Cuentas'],
            'limit' => 2000
        ];
        $clientes = $this->paginate($this->Clientes);
        $activado = [1 => 'Sí', 0 => 'No'];
        $this->set(compact('clientes', 'activado'));
        $this->set('_serialize', ['clientes']);
    }

    /**
     * View method
     *
     * @param string|null $id Cliente id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $cliente = $this->Clientes->get($id, [
            'contain' => ['Provincias', 'Paises', 'Telefonos', 'Personas', 'Cuentas']
        ]);
        $cuentasCliente = new CuentasCliente();
        $Movimientos = TableRegistry::get('Movimientos');
        foreach ($cliente->cuentas as $cta) {
            $query = $Movimientos->find()
                    ->where(['cuenta_id'=> $cta->id]);
            $saldo= $cuentasCliente->get_saldo($query);
            $cta->saldo=$saldo;
        }

        $this->set('cliente', $cliente);
        $this->set('_serialize', ['cliente']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $cliente = $this->Clientes->newEntity();
        if ($this->request->is('post')) {
            $cliente = $this->Clientes->patchEntity($cliente, $this->request->getData(), [
                'associated' => ['Telefonos', 'Personas']]);
            if ($this->Clientes->save($cliente)) {
                $this->Flash->success(__('La información ha sido guardada.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo guardar la información.'));
        }
        $provincias = $this->Clientes->Provincias->find('list', ['limit' => 200]);
        $paises = $this->Clientes->Paises->find('list', ['limit' => 200]);
        $personas = $this->Clientes->Personas->find('list', ['limit' => 20000]);
        $this->set(compact('cliente', 'provincias', 'paises', 'personas'));
        $this->set('_serialize', ['cliente']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cliente id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $cliente = $this->Clientes->get($id, [
            'contain' => ['Telefonos', 'Personas']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cliente = $this->Clientes->patchEntity($cliente, $this->request->getData(), [
                'associated' => ['Telefonos', 'Personas']]);
            if ($this->Clientes->save($cliente)) {
                $this->Flash->success(__('La información ha sido editada.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo editar la información.'));
        }
        $provincias = $this->Clientes->Provincias->find('list', ['limit' => 200]);
        $paises = $this->Clientes->Paises->find('list', ['limit' => 200]);
        $personas = $this->Clientes->Personas->find('list', ['limit' => 20000]);
        $this->set(compact('cliente', 'provincias', 'paises', 'personas'));
        $this->set('_serialize', ['cliente']);
    }

    public function activar($id = null) {
        $cliente = $this->Clientes->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cliente->activo = 1;
            if ($this->Clientes->save($cliente)) {
                $this->Flash->success(__('El cliente fue activado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo activar el cliente.'));
        }
    }

    public function desactivar($id = null) {
        $cliente = $this->Clientes->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cliente->activo = 0;
            if ($this->Clientes->save($cliente)) {
                $this->Flash->success(__('El cliente fue desactivado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo activar el cliente.'));
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Cliente id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $cliente = $this->Clientes->get($id);
        if ($this->Clientes->delete($cliente)) {
            $this->Flash->success(__('La información ha sido borrada.'));
        } else {
            $this->Flash->error(__('No se pudo borrar la información, inténtelo nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
