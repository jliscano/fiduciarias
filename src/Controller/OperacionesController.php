<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Operaciones Controller
 *
 * @property \App\Model\Table\OperacionesTable $Operaciones
 *
 * @method \App\Model\Entity\Operacione[] paginate($object = null, array $settings = [])
 */
class OperacionesController extends AppController
{


  public function initialize()
  {
      parent::initialize();

      $this->loadComponent('RequestHandler');

      }




    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $operaciones = $this->paginate($this->Operaciones);
        $cerra = [0 => 'No', 1 => 'Sí'];
        $this->set(compact('operaciones', 'cerra'));
        $this->set('_serialize', ['operaciones']);
    }


    public function batch()
    {
        $this->paginate = [
            'limit' => 10000,
            'contain' => ['Users', 'Movimientos'],
            'where' => ['Operaciones.tipooperacione_id' => 2 ],
            'order' => ['Operaciones.id' => 'desc']
        ];


        $operaciones = $this->paginate($this->Operaciones->findByTipooperacione_id(1));
        $cerra = [0 => 'Abierto', 1 => 'Cerrado'];
        $this->set(compact('operaciones', 'cerra'));
        $this->set('_serialize', ['operaciones']);
    }

    public function cierresemanal()
    {

        $this->paginate = [
            'contain' => ['Users'],
            'where' => ['Operaciones.tipooperacione_id' => 5 ]
        ];
        $operaciones = $this->paginate($this->Operaciones->findByTipooperacione_id(2));
        $cerra = [0 => 'No', 1 => 'Sí'];
        $this->set(compact('operaciones', 'cerra'));
        $this->set('_serialize', ['operaciones']);
    }





    public function view($id = null)
    {
    	        	echo "<script>alert('ooo');</script>";
        $operacione = $this->Operaciones->get($id, [
            'contain' => ['Users', 'Movimientos' => ['Cuentas' => ['Bancos'], 'Clientes', 'Tipotransacciones']]
        ]);



        $tipotransacciones = $this->Operaciones->Movimientos->Tipotransacciones->find('list', ['limit' => 200]);

        $this->set('operacione', $operacione);
        $this->set('_serialize', ['operacione']);
        $this->set(compact('tipotransacciones'));
        }


        public function versemana($id = null)
        {
            $operacione = $this->Operaciones->get($id, [
                'contain' => ['Users', 'Movimientos' => ['Cuentas', 'Clientes']]
            ]);





            $this->set('operacione', $operacione);
            $this->set('_serialize', ['operacione']);
            }


            public function nuevomovimiento($id = null)
            {
                $operacione = $this->Operaciones->get($id, [
                    'contain' => ['Users', 'Movimientos' => ['Cuentas' => ['Bancos'], 'Clientes', 'Tipotransacciones']]
                ]);



                $tipotransacciones = $this->Operaciones->Movimientos->Tipotransacciones->find('list', ['limit' => 200]);
                $clientes = $this->Operaciones->Movimientos->Clientes->find('list', ['limit' => 2000]);
                $cuentas = $this->Operaciones->Movimientos->Cuentas->find('list', ['limit' => 2000]);
                $this->set('operacione', $operacione);
                $this->set('_serialize', ['operacione']);
                $this->set(compact('tipotransacciones', 'clientes', 'cuentas'));
                }




    public function add()
    {
        $operacione = $this->Operaciones->newEntity();
        if ($this->request->is('post')) {
            $operacione->user_id = $this->Auth->user('id');


            $operacione = $this->Operaciones->patchEntity($operacione, $this->request->getData(), [
                'associated' => ['Movimientos']]);
            if ($this->Operaciones->save($operacione)) {
                $this->Flash->success(__('La operación ha sido guardada.'));

                return $this->redirect(['controller' => 'movimientos', 'action' => 'index']);
            }
            $this->Flash->error(__('The operacione could not be saved. Please, try again.'));
        }

        $activarscriptespecial = 1;
        $this->set(compact('operacione', 'users', 'activarscriptespecial'));
        $this->set('_serialize', ['operacione']);
    }

    public function nuevobatch()
    {

        //$this->viewBuilder()->setLayout(false);
        $operacione = $this->Operaciones->newEntity();
        if ($this->request->is('post')) {
            $operacione->user_id = $this->Auth->user('id');
            $operacione->tipooperacione_id = 1;
            $operacione->descripcion = "bacth";

            $operacione = $this->Operaciones->patchEntity($operacione, $this->request->getData(), [
                'associated' => ['Movimientos']]);
            if ($this->Operaciones->save($operacione)) {
                $this->Flash->success(__('El batch ha sido guardado.'));

                return $this->redirect(['controller' => 'operaciones', 'action' => 'batch']);
            }
            $this->Flash->error(__('The operacione could not be saved. Please, try again.'));
        }

        $activarscriptespecial = 1;
        $this->set(compact('operacione', 'users', 'activarscriptespecial'));
        $this->set('_serialize', ['operacione']);
    }




    public function nuevocierresemanal()
    {
        $operacione = $this->Operaciones->newEntity();
        if ($this->request->is('post')) {
            $operacione->user_id = $this->Auth->user('id');
            $operacione->tipooperacione_id = 2;

            $operacione = $this->Operaciones->patchEntity($operacione, $this->request->getData(), [
                'associated' => ['Movimientos']]);
            if ($this->Operaciones->save($operacione)) {
                $this->Flash->success(__('El cierre ha sido guardado.'));

                return $this->redirect(['controller' => 'operaciones', 'action' => 'batch']);
            }
            $this->Flash->error(__('The operacione could not be saved. Please, try again.'));
        }

        $activarscriptespecial = 1;
        $this->set(compact('operacione', 'users', 'activarscriptespecial'));
        $this->set('_serialize', ['operacione']);
    }



    public function cerrarbatch($id = null)
    {
        $operacione = $this->Operaciones->get($id, [
            'contain' => ['Users', 'Movimientos' => 'Cuentas']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $operacione->cerrado = 1;

            if ($this->Operaciones->save($operacione)) {
                $this->Flash->success(__('El batch ha sido cerrado.'));

                return $this->redirect(['action' => 'batch']);
            }
            $this->Flash->error(__('The operacione could not be saved. Please, try again.'));
        }

        $this->set(compact('operacione'));
        $this->set('_serialize', ['operacione']);
    }


    public function abrirbatch($id = null)
    {
        $operacione = $this->Operaciones->get($id, [
            'contain' => ['Users', 'Movimientos' => 'Cuentas']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $operacione->cerrado = 0;

            if ($this->Operaciones->save($operacione)) {
                $this->Flash->success(__('El batch ha sido abierto.'));

                return $this->redirect(['action' => 'batch']);
            }
            $this->Flash->error(__('The operacione could not be saved. Please, try again.'));
        }

        $this->set(compact('operacione'));
        $this->set('_serialize', ['operacione']);
    }



    public function cerrarsemana($id = null)
    {
        $operacione = $this->Operaciones->get($id, [
            'contain' => ['Users', 'Movimientos' => 'Cuentas']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $operacione->cerrado = 1;

            if ($this->Operaciones->save($operacione)) {
                $this->Flash->success(__('La semana ha sido cerrada.'));

                return $this->redirect(['action' => 'cierresemanal']);
            }
            $this->Flash->error(__('The operacione could not be saved. Please, try again.'));
        }

        $this->set(compact('operacione'));
        $this->set('_serialize', ['operacione']);
    }




    public function edit($id = null)
    {
      $operacione = $this->Operaciones->get($id, [
          'contain' => ['Users', 'Movimientos' => ['Cuentas' => ['Bancos'], 'Tipotransacciones', 'Clientes']]
      ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $operacione = $this->Operaciones->patchEntity($operacione, $this->request->data, [
                'associated' => ['Movimientos'=> ['Cuentas', 'Tipotransacciones']]]);
            if ($this->Operaciones->save($operacione)) {
                $this->Flash->success(__('La información ha sido guardada.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The operacione could not be saved. Please, try again.'));
        }
        $cuentas = $this->Operaciones->Movimientos->Cuentas->find('list', ['limit' => 2000]);
      //  $clientes = $this->Operaciones->Movimientos->Clientes->find('list', ['limit' => 2000]);
        $tipotransacciones = $this->Operaciones->Movimientos->Tipotransacciones->find('list', ['limit' => 200]);
        $this->set(compact('operacione', 'cuentas', 'tipotransacciones'));
        $this->set('_serialize', ['operacione']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Operacione id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $operacione = $this->Operaciones->get($id);
        if ($this->Operaciones->delete($operacione)) {
            $this->Flash->success(__('The operacione has been deleted.'));
        } else {
            $this->Flash->error(__('The operacione could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }




    public function clientes() {

        $this->viewBuilder()->layout('ajax');
        $this->LoadModel('Clientesdetallados');
        $cuenta = $this->request->data['cuenta_id'];
        $clientes = $this->Clientesdetallados->find('list', [
         'limit' => 200,
         'keyField' => 'cliente_id',
         'conditions' => ['Clientesdetallados.cuenta_id' => $cuenta],
         'contain' => ['Cuentas' => ['Bancos', 'Monedas'] ]

        ]);

      //$clientes = $this->Clientesdetallados->find('list');
        $this->set(compact('clientes'));
        $this->set('_serialize', 'clientes');

    }

}
