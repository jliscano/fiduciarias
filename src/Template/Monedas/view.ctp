<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item"><a href="/monedas">Monedas</a> </li>
  <li class="breadcrumb-item active"> <?= h($moneda->moneda) ?></li>
</ol>
<div class="card">
  <div class="card-block">
    <h3><?= h($moneda->moneda) ?></h3>
    <table class="table">
        <tr>
            <th scope="row"><?= __('Moneda') ?></th>
            <td><?= h($moneda->moneda) ?></td>
        </tr>


        
      
    </table>
    <div class="related">
        <h4>Cuentas que usan la moneda <?= h($moneda->moneda) ?></h4>
        <?php if (!empty($moneda->cuentas)): ?>
        <div class="table-responsive">
          <table class="table" id="table_id">
            <thead>

               
                    <th scope="col"><?= __('N° de cuenta') ?></th>
                <th scope="col"><?= __('Tipo de cuenta') ?></th>


                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </thead>
            <?php foreach ($moneda->cuentas as $cuentas): ?>
            <tr>

              
                  <td><?= h($cuentas->numero) ?></td>
                <td><?= h($cuentas->tipocuenta->tipocuenta) ?></td>


                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Cuentas', 'action' => 'view', $cuentas->id]) ?>

            </tr>
            <?php endforeach; ?>
        </table>
        </div>
        <?php endif; ?>
    </div>
  </div>
</div>
