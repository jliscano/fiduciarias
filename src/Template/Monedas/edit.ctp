<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item"><a href="/monedas">Monedas</a> </li>
  <li class="breadcrumb-item active"> Editar: <?= h($moneda->moneda) ?></li>
</ol>
<div class="card">
  <div class="card-header">
  <legend><?= __('Editar Moneda') ?></legend>
  </div>
  <div class="card-block">
    <?= $this->Form->create($moneda) ?>

      <div class="form-group">
        <?php echo $this->Form->control('moneda', ['class' => 'form-control']); ?>
      </div>
    


    <?= $this->Form->button(__('Editar'), ['class' => 'btn btn-primary btn-lg']) ?>
    <?= $this->Form->end() ?>
  </div>
</div>
