<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>

  <li class="breadcrumb-item active"> Monedas</li>
</ol>

<ul class="nav">

        <li class="list-group-item"><?= $this->Html->link(__('Nueva moneda'), ['action' => 'add' ]) ?></li>


    </ul>

<div class="card">
 <div class="card-header">
    <h3><?= __('Monedas') ?></h3>
    </div>
    <div class="card-block">
    <div class="table-responsive">
    <table class="table" id="table_id">
        <thead>
            <tr>

                <th><?= $this->Paginator->sort('moneda') ?></th>


                <th class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($monedas as $moneda): ?>
            <tr>

                <td><?= h($moneda->moneda) ?></td>
              

                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $moneda->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $moneda->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $moneda->id], ['confirm' => __('Are you sure you want to delete # {0}?', $moneda->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
  </div>
</div>
