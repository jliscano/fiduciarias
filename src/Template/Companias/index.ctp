<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item active">Compañías administradoras </li>

</ol>
<?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
<ul class="nav">

        <li class="list-group-item"><?= $this->Html->link(__('Nueva Compañía'), ['action' => 'add' ]) ?></li>


    </ul>
<?php endif; ?>
<div class="card">
 <div class="card-header">
    <h3><?= __('Compañías administradoras') ?></h3>
    </div>
    <div class="card-block">
    <div class="table-responsive">
    <table class="table" id="table_id">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nombre') ?></th>
               
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($companias as $compania): ?>
            <tr>
                <td><?= $this->Number->format($compania->id) ?></td>
                <td><?= h($compania->nombre) ?></td>
              
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $compania->id]) ?>
                    <?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $compania->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $compania->id], ['confirm' => __('Are you sure you want to delete # {0}?', $compania->id)]) ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
      </table>
      </div>
     </div>
  </div>
