<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item"><a href="/companias">Compañías Administradoras</a> </li>
  <li class="breadcrumb-item active"> <?= h($compania->nombre) ?></li>
</ol>
<div class="card">
<div class="card-header">
<h3><?= h($compania->nombre) ?></h3>
</div>
<div class="card-block">

    <table class="table">
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($compania->nombre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Descripción') ?></th>
            <td>  <?= $this->Text->autoParagraph(h($compania->descripcion)); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha de creación') ?></th>
            <td><?= h($compania->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Última modificación') ?></th>
            <td><?= h($compania->modified) ?></td>
        </tr>
    </table>

    <div class="related">
        <h4>Cuentas con la compañía <?= h($compania->nombre) ?></h4>
        <?php if (!empty($compania->cuentas)): ?>
        <div class="table-responsive">
        <table class="table" id="table_id">
            <thead>

                <th scope="col"><?= __('Cliente') ?></th>

                <th scope="col"><?= __('Banco') ?></th>
                <th scope="col"><?= __('Numero') ?></th>
                <th scope="col"><?= __('Vencimiento') ?></th>

                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </thead>
            <?php foreach ($compania->cuentas as $cuentas): ?>
            <tr>

                <td><?= h($cuentas->cliente->nombre) ?></td>
                <td><?= h($cuentas->banco->nombre) ?></td>


                <td><?= h($cuentas->numero) ?></td>

                <td><?= h($cuentas->vencimiento) ?></td>

                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Cuentas', 'action' => 'view', $cuentas->id]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        </div>
        <?php endif; ?>
    </div>
</div>
</div>
