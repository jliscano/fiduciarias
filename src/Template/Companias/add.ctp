<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item"><a href="/companias">Compañías Administradoras</a> </li>
  <li class="breadcrumb-item active"> Nuevo</li>
</ol>

<div class="card">
  <div class="card-header">
  <legend><?= __('Nueva Compañía') ?></legend>
  </div>
      <div class="card-block">
    <?= $this->Form->create($compania) ?>

       <div class="form-group">
        <?php echo $this->Form->control('nombre', ['class' => 'form-control']); ?>
      </div>
       <div class="form-group">
        <?php echo $this->Form->control('descripcion', ['class' => 'form-control']); ?>
      </div>

    <?= $this->Form->button(__('Guardar'), ['class' => 'btn btn-primary btn-lg']) ?>
    <?= $this->Form->end() ?>
  </div>
</div>
