
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item active ">Usuarios</li>
</ol>
  <ul class="nav">

        <li class="list-group-item"><?= $this->Html->link(__('Nuevo Usuario'), ['action' => 'add']) ?></li>
    </ul>

    <div class="card">
     <div class="card-header">
        <h3><?= __('Usuarios') ?></h3>
        </div>
        <div class="card-block">
        <table class="table" id="table_id">
        <thead>
            <tr>

                <th scope="col"><?= $this->Paginator->sort('username', 'Usuario') ?></th>

                <th scope="col"><?= $this->Paginator->sort('role', 'Rol') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created', 'Fecha de creación') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified', 'Última modificación') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>

                <td><?= h($user->username) ?></td>

                <td><?= h($user->role) ?></td>
                <td><?= h($user->created) ?></td>
                <td><?= h($user->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $user->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $user->id]) ?>
                    <?= $this->Html->link(__('Cambiar contraseña'), ['action' => 'cambiarpassword', $user->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
      </table>
      </div>
   </div>
