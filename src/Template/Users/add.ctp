<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item"><a href="/users">Usuarios</a> </li>
  <li class="breadcrumb-item active">Nuevo</li>
</ol>
<div class="card">
<div class="card-header" >
           <legend><?= __('Nuevo Usuario') ?></legend>
    </div>
    <?= $this->Form->create($user) ?>
      <div class="card-block">
         <div class="form-group">
        <?php    echo $this->Form->control('username', ['class' => 'form-control', 'label' => 'Usuario']);?>
        </div>
         <div class="form-group">
        <?php    echo $this->Form->control('password', ['class' => 'form-control', 'label' => 'Contraseña']);?>
        </div>
         <div class="form-group">
        <?php    echo $this->Form->control('role', ['class' => 'form-control', 'label' => 'Privilegio', 'options' => $privilegios]);?>
        </div>


    <?= $this->Form->button(__('Guardar'), ['class' => 'btn btn-primary btn-lg']) ?>
    <?= $this->Form->end() ?>
  </div>
  </div>
