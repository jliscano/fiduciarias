<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item"><a href="/users">Usuarios</a> </li>
  <li class="breadcrumb-item active">Cambiar contraseña</li>
</ol>

<div class="card">
<div class="card-header" >
           <legend><?= __('Cambiar contraseña ') ?> al usuario <?= h($user->username) ?></legend>
    </div>
<?= $this->Form->create() ?>
  <div class="card-block">
 <div class="form-group">
    <?= $this->Form->input('password1',['type'=>'password' ,'label'=>'Nueva contraseña', 'class' => 'form-control']) ?>
  </div>
   <div class="form-group">
    <?= $this->Form->input('password2',['type' => 'password' , 'label'=>'Vuelva a introducir la misma contraseña', 'class' => 'form-control'])?>
</div>
<?= $this->Form->button(__('Cambiar contraseña'), ['class' => 'btn btn-primary btn-lg']) ?>
<?= $this->Form->end() ?>
</div>
</div>
