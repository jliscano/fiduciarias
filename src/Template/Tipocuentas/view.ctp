<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item"><a href="/tipocuentas">Tipos de Cuentas</a> </li>
  <li class="breadcrumb-item active"> <?= h($tipocuenta->tipocuenta) ?></li>
</ol>
<div class="card">
  <div class="card-header">
<h3 st> Tipo de Cuenta: <?= h($tipocuenta->tipocuenta) ?></h3>
  </div>
    <div class="card-block">

    <div class="related">
        <h4>Cuentas de <?= h($tipocuenta->tipocuenta) ?> registradas  </h4>
        <?php if (!empty($tipocuenta->cuentas)): ?>
        <table class="table"  id="table_id">
            <thead>

                <th><?= __('Número') ?></th>

                <th><?= __('Moneda') ?></th>
                <th><?= __('Saldo') ?></th>
                <th><?= __('Tasa') ?></th>
                <th><?= __('Plazo') ?></th>
              
                <th> Acciones </th>


            </thead>
            <?php foreach ($tipocuenta->cuentas as $cuentas): ?>
            <tr>

                <td><?= h($cuentas->numero) ?></td>

                <td><?= h($cuentas->moneda->moneda) ?></td>
                <td><?= h($cuentas->saldo) ?></td>
                <td><?= h($cuentas->tasa) ?></td>
                <td><?= h($cuentas->plazo) ?></td>


                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Cuentas', 'action' => 'view', $cuentas->id]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
  </div>
</div>
