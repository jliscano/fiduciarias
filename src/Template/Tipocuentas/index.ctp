<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item active">Tipo de Cuentas</li>
</ol>
<?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
<ul class="nav">

        <li class="list-group-item"><?= $this->Html->link(__('Nuevo tipo de Cuenta'), ['action' => 'add' ]) ?></li>


    </ul>
<?php endif; ?>
<div class="card">
 <div class="card-header">
    <h3><?= __('Tipo de cuentas') ?></h3>
    </div>
    <div class="card-block">
    <table class="table" id="table_id">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tipocuenta', 'Tipo de cuenta') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tipocuentas as $tipocuenta): ?>
            <tr>
                <td><?= $this->Number->format($tipocuenta->id) ?></td>
                <td><?= h($tipocuenta->tipocuenta) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $tipocuenta->id]) ?>
                    <?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $tipocuenta->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $tipocuenta->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tipocuenta->id)]) ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
      </table>

      </div>
  </div>
