<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item"><a href="/tipocuentas">Tipo de Cuentas</a> </li>
  <li class="breadcrumb-item active">Nueva</li>
</ol>

<div class="card">

    <div class="card-header" >
        <legend><?= __('Nuevo tipo de Cuenta') ?></legend>
      </div>
     <div class="card-block">
       <div class="tipocuentas form large-9 medium-8 columns content">
    <?= $this->Form->create($tipocuenta) ?>

      <div class="form-group">
        <?php
            echo $this->Form->control('tipocuenta', ['label' => 'Tipo de Cuenta', 'class' => 'form-control']);
        ?>

      </div>
    </fieldset>
      <?= $this->Form->button(__('Guardar'), ['class' => 'btn btn-primary btn-lg']) ?>
    <?= $this->Form->end() ?>
  </div>
  </div>

  </div>
