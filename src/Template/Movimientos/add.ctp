
<div class="card">
  <div class="card-header">
  <legend><?= __('Nuevo movimiento') ?></legend>
  </div>
  <div class="card-block">
    <?= $this->Form->create($movimiento) ?>
    <div class="row">
      <div class="form-group col-3">
          <?php echo $this->Form->control('operacione_id', [ 'class' => 'form-control', 'value' => 17, 'type' => 'text']); ?>
      </div>
    <div class="form-group col-3">
        <?php echo $this->Form->control('cliente_id', ['options' => $clientes, 'class' => 'form-control']); ?>
    </div>
    <div class="form-group col-3">
        <?php echo $this->Form->control('cuenta_id', ['options' => $cuentas, 'class' => 'form-control']); ?>
    </div>
    <div class="form-group col-3">
        <?php echo $this->Form->control('monto', [ 'class' => 'form-control']); ?>
        </div>
    <div class="form-group col-3">
        <?php echo $this->Form->control('tipotransacciones_id', ['options' => $tipotransacciones, 'class' => 'form-control']); ?>
        </div>


        <div class="form-group col-3">
        <?php echo $this->Form->control('created', ['label' => 'Fecha', 'class' => 'form-control', 'id' => 'datepicker', 'type' => 'text']); ?>
        </div>

        </div>
        <?= $this->Form->button(__('Guardar'), ['class' => 'btn btn-primary btn-lg']) ?>

    <?= $this->Form->end() ?>
</div>
