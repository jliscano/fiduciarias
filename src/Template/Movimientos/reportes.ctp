<style media="screen">
  .dataTables_filter, .oculto { display: none; }
</style>
<div class="card">
 <div class="card-header">
    <h3><?= __('Reportes') ?></h3>
    </div>
    <div class="card-block">
    <div class="table-responsive">
    <table class="table table-striped table-bordered">
    <tr>

<td id="batchre"></td>
<td id="cuentare"></td>
<td id="bancore"></td>
<td id="clientere"></td>




</tr>

<tr>
<td id="montore"></td>
<td id="monedare"></td>
<td id="descripcionre" class="oculto"></td>
<td id="transaccionre"></td>
<td id="fechare">Fecha</td>


</tr>


</table>
    <table class="table table-sm" id="tabla-reportes">
        <thead>

            <tr>

             	<th scope="col"><?= $this->Paginator->sort('operacione_id', 'Batch') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cuenta_id', 'Cuenta') ?></th>
                <th scope="col"><?= $this->Paginator->sort('banco_id', 'Banco') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cliente_id', 'Cliente') ?></th>
                <th scope="col"><?= $this->Paginator->sort('monto') ?></th>
                <th scope="col"><?= $this->Paginator->sort('moneda') ?></th>
                <th scope="col"><?= $this->Paginator->sort('descripcion') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tipotransacciones_id', 'Tipo de transacción') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created', 'Fecha') ?></th>

            </tr>


        </thead>



        <tbody>




            <?php foreach ($movimientos as $movimiento): ?>
            <tr>
             <td><?= $movimiento->operacione->id ?></td>

                <td><?= $movimiento->cuenta->numero ?></td>
                <td><?= $movimiento->cuenta->banco->nombre ?></td>
                  <td><?= $movimiento->cliente->nombre ?></td>

                <td><?= $this->Number->format($movimiento->monto) ?></td>
                <td><?= $movimiento->cuenta->moneda->moneda ?></td>
                <td class="col-3"><?= $movimiento->descripcion ?></td>
                <td><?= $movimiento->tipotransaccione->transaccion ?></td>
                <td><?= h($movimiento->created->format('d-m-Y')) ?></td>



            </tr>
            <?php endforeach; ?>
        </tbody>
        </table>
        </div>
        </div>
     </div>
