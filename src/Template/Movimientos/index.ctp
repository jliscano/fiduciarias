



<div class="card">
 <div class="card-header">
    <h3><?= __('Movimientos') ?></h3>
    </div>
    <div class="card-block">
    <div class="table-responsive">
    <table class="table" id="table_id">
        <thead>
            <tr>

             	 <th scope="col"><?= $this->Paginator->sort('operacione_id', 'Batch') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cuenta_id', 'Cuenta') ?></th>
                 <th scope="col"><?= $this->Paginator->sort('cuenta_id', 'Banco') ?></th>
                <th scope="col"><?= $this->Paginator->sort('monto') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('descripcion') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tipotransacciones_id', 'Tipo de transacción') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created', 'Fecha de creación') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($movimientos as $movimiento): ?>
            <tr>
             <td><?= $movimiento->has('operacione') ? $this->Html->link($movimiento->operacione->id, ['controller' => 'Operaciones', 'action' => 'view', $movimiento->operacione->id]) : '' ?></td>

                <td><?= $movimiento->has('cuenta') ? $this->Html->link($movimiento->cuenta->numero, ['controller' => 'Cuentas', 'action' => 'view', $movimiento->cuenta->id]) : '' ?></td>
                <td><?= $movimiento->has('cuenta') ? $this->Html->link($movimiento->cuenta->banco->nombre, ['controller' => 'Bancos', 'action' => 'view', $movimiento->cuenta->banco->id]) : '' ?></td>
                <td><?= $this->Number->format($movimiento->monto) ?></td>
                <td><?= $movimiento->descripcion ?></td>
                <td><?= $movimiento->has('tipotransaccione') ? $this->Html->link($movimiento->tipotransaccione->transaccion, ['controller' => 'Tipotransacciones', 'action' => 'view', $movimiento->tipotransaccione->id]) : '' ?></td>
                <td><?=  h($movimiento->created) ?></td>


                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $movimiento->id]) ?>
                    <?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $movimiento->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $movimiento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $movimiento->id)]) ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        </table>
        </div>
        </div>
     </div>
