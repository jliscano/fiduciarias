<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item"><a href="/movimientos">Movimientos</a> </li>
  <li class="breadcrumb-item active"><?= h($movimiento->id) ?></li>
</ol>
<div class="card">
  <div class="card-block">
<?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
    <ul class="nav">

        <li class="list-group-item"><?= $this->Html->link(__('Editar'), ['action' => 'edit', $movimiento->id]) ?> </li>
        <li class="list-group-item"><?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $movimiento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $movimiento->id)]) ?> </li>

    </ul>
<?php endif; ?>
</nav>
<div class="movimientos view large-9 medium-8 columns content">
    <h3>Movimiento N° <?= h($movimiento->id) ?></h3>
    <table class="table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= h($movimiento->id) ?></td>
        </tr>

        <tr>
            <th scope="row"><?= __('Cuenta') ?></th>
            <td><?= $movimiento->has('cuenta') ? $this->Html->link($movimiento->cuenta->numero, ['controller' => 'Cuentas', 'action' => 'view', $movimiento->cuenta->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tipo de transacción') ?></th>
            <td><?= $movimiento->tipotransaccione->transaccion ?></td>
        </tr>
        <tr>
            <th scope="row">Batch</th>
            <td><?=  $this->Html->link($movimiento->operacione->id, ['controller' => 'Operaciones', 'action' => 'view', $movimiento->operacione->id])  ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Monto') ?></th>
            <td>


            <?= $this->Number->format($movimiento->monto) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Descripción') ?></th>
            <td><?= h($movimiento->descripcion) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha de creación') ?></th>
            <td><?= h($movimiento->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Última modificación') ?></th>
            <td><?= h($movimiento->modified) ?></td>
        </tr>
    </table>
    </div>
    </div>
</div>
