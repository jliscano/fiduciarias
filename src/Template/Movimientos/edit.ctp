

<div class="card">
  <div class="card-header">
    <?= $this->Form->create($movimiento) ?>
  </div>
  <div class="card-block">
        <legend><?= __('Editar') ?></legend>
        <div class="row">

          <div class="form-group col-3">
        <?php echo $this->Form->control('cuenta_id', ['options' => $cuentas, 'class' => 'form-control']);   ?>
      </div>
      <div class="form-group col-3">
        <?php echo $this->Form->control('monto', ['class' => 'form-control', 'type' => 'text']);   ?>
      </div>
      <div class="form-group col-3">
        <?php echo $this->Form->control('tipotransacciones_id', ['label'=> 'Tipo de transacción', 'options' => $tipotransacciones, 'class' => 'form-control']);   ?>
      </div>
      <div class="form-group col-3">
        <?php echo $this->Form->control('descripcion', ['label'=> 'Descripción', 'class' => 'form-control']);   ?>
      </div>

      </div>
    </div>
      <?= $this->Form->button(__('Editar'), ['class' => 'btn btn-primary btn-lg']) ?>
    <?= $this->Form->end() ?>
</div>
