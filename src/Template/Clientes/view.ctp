<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item"><a href="/clientes">Clientes</a></li>
<li class="breadcrumb-item active"><?= h($cliente->nombre) ?></li>
</ol>
<div class="card">
<div class="card-header">
<h3>Cliente: <?= h($cliente->nombre) ?></h3>
</div>
    <div class="card-block">
    <table class="table">
    <tr>
    <th>Cuenta(s)</th>
        <td>
          
        <ul class="list-group">
        <?php foreach ($cliente->cuentas as $cuentas): ?>
        <li class="list-group-item">  <a href="/cuentas/view/<?= h($cuentas->id) ?>"><?= h($cuentas->numero) ?> Saldo <?= $this->Number->precision($cuentas->saldo, 2) ?></a></li>
        <?php endforeach; ?>
        </ul>
        </td>
        </tr>
        <tr>
         <th>Nombre de cliente</th>
        <td>
        <?php foreach ($cliente->personas as $personas): ?>
        <li class="list-group-item">  <a href="/personas/view/<?= h($personas->id) ?>"><?= h($personas->nombre) ?></a></li>
        <?php endforeach; ?>
        </td>
        </tr>
        <tr>
        <th scope="row"><?= __('Nombre') ?></th>
        <td><?= h($cliente->nombre) ?></td>
        </tr>
        <tr>
        <th scope="row"><?= __('Nro Cliente') ?></th>
        <td><?= h($cliente->nrocliente) ?></td>
        </tr>
        <tr>
        <th scope="row"><?= __('País') ?></th>
        <td><?= $cliente->has('paise') ? $this->Html->link($cliente->paise->pais, ['controller' => 'Paises', 'action' => 'view', $cliente->paise->id]) : '' ?></td>
        </tr>
        <tr>
        <th scope="row"><?= __('Fecha de creación') ?></th>
        <td><?= h($cliente->created) ?></td>
        </tr>
        <tr>
        <th scope="row"><?= __('Última modificación') ?></th>
        <td><?= h($cliente->modified) ?></td>
        </tr>
        <tr>
        <th> Dirección</th>
        <td> <?= $this->Text->autoParagraph(h($cliente->direccion)); ?></td>
        </tr>
        <tr>
        <th>Teléfono(s)</th>
        <td>
        <ul class="list-group">
        <?php foreach ($cliente->telefonos as $telefonos): ?>
         <li class="list-group-item">  <?= h($telefonos->telefono) ?></li>
        <?php endforeach; ?>
        </ul>
        </td>
        </tr>
    </table>

 <?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
         <?= $this->Html->link(__('Editar'), ['action' => 'edit', $cliente->id]) ?>
<?php endif; ?>

</div>
</div>
