<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item"><a href="/clientes">Clientes</a></li>
<li class="breadcrumb-item active">Editar : <?= h($cliente->nombre) ?></li>
</ol>

<div class="card">
   <?= $this->Form->create($cliente) ?>

   <div class="card-header" >
       <legend><?= __('Nuevo Cliente') ?></legend>
   </div>
    <div class="card-block">
   <div class="form-group" id="prefetch">
       <?php
           echo $this->Form->control('nombre', ['label' => 'Nombre de la empresa', 'class' => 'form-control']); ?>
           </div>


           <div class="form-group">
<label>Nombre de cliente</label>
<?php echo $this->Form->control('personas._ids', ['id' => 'tokenize-sortable1', 'label' => false]); ?>


         </div>
           <div class="form-group">
               <?php
                   echo $this->Form->control('nrocliente', ['label' => 'Nro de Cliente', 'class' => 'form-control', 'type' => 'number']); ?>
                   </div>



       <div class="form-group">
          <?php echo $this->Form->control('paise_id', ['options' => $paises, 'label' => 'País', 'empty' => 'Seleccione el país', 'class' => 'form-control']); ?>
          </div>


          <div class="form-group">
         <?php  echo $this->Form->control('direccion', ['label' => 'Dirección', 'class' => 'form-control']); ?>
         </div>




           <legend>Teléfono</legend>


<table class="table" id="grade-table">
       <thead>
           <tr>

               <th></th>
               <th>&nbsp;</th>
           </tr>
       </thead>
       <tbody></tbody>
       <tfoot>
           <tr>

               <td class="actions">
                   <a href="#" class="add" title="Agregar"><div class="btn btn-success">  <i class="fa fa-check-circle fa-lg"></i></div></a>
               </td>
           </tr>
       </tfoot>
   </table>

<script id="grade-template" type="text/x-underscore-template">
   <?php echo $this->element('telefonos');?>
</script>


    <?= $this->Form->button(__('Editar'), ['class' => 'btn btn-primary btn-lg']) ?>
    <?= $this->Form->end() ?>

 </div>

</div>


<div class="jumbotron">
<h3> Teléfonos </h3>
         <ul class="list-group">
                <?php foreach ($cliente->telefonos as $telefonos): ?>
                <li class="list-group-item"><i class="icon-phone icons font-2xl d-block mt-4"></i>  <?= h($telefonos->telefono) ?>   <?= $this->Form->postLink('Borrar', ['controller' => 'telefonos', 'action' => 'delete', $telefonos->id], ['class'=> 'btn btn-danger btn-sm', 'confirm' => __('Desea borrar el teléfono  {0}?', $telefonos->id)]) ?></li>
                <?php endforeach; ?>
</ul>
</div>
