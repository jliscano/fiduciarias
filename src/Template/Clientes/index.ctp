<!-- Breadcrumb -->
<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">Clientes</li>
            </ol>
            <?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
<ul class="nav">

        <li class="list-group-item"><?= $this->Html->link(__('Nuevo cliente'), ['action' => 'add' ]) ?></li>


    </ul>
<?php endif; ?>
<div class="card">

 <div class="card-header">
    <h3><?= __('Clientes') ?></h3>
    </div>
    <div class="card-block">
    <div class="table-responsive">
    <table class="table" id="clientes">
        <thead>
            <tr>

                <th scope="col"><?= $this->Paginator->sort('nombre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nrocliente', 'N° Cliente') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('activado', 'Activado') ?></th>


                <th scope="col"><?= $this->Paginator->sort('paise_id', 'País') ?></th>

                  <th scope="col">Teléfono(s)</th>
                <th scope="col"><?= $this->Paginator->sort('modified', 'Ultima modificación') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($clientes as $cliente): ?>
            <tr>

                <td><?= h($cliente->nombre) ?></td>
                <td><?= h($cliente->nrocliente) ?></td>
                  <td><?= $activado[$cliente->activo] ?></td>

                <td><?= $cliente->paise->pais ?></td>
                   <td>

                   <?php foreach ($cliente->telefonos as $telefonos): ?>
                    <em class="badge badge-light"> <?= h($telefonos->telefono) ?></em>
                    <?php endforeach; ?>

                </td>

                <td><?= h($cliente->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $cliente->id]) ?>
               <?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $cliente->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $cliente->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cliente->id)]) ?>
                    <?php if ($cliente->activo == 1){ ?>
                      <?= $this->Form->postLink(__('Desactivar'), ['action' => 'desactivar', $cliente->id], ['confirm' => __('Desea desactivar al cliente  # {0}?', $cliente->id)]) ?>
                    <?php } else { ?>
  <?= $this->Form->postLink(__('Activar'), ['action' => 'activar', $cliente->id], ['confirm' => __('Desea activar al cliente  # {0}?', $cliente->id)]) ?>

                    <?php } ?>
                  <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
   </div>
</div>
