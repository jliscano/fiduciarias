<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item"><a href="/cuentas">Cuentas</a> </li>
  <li class="breadcrumb-item active">Editar: <?= h($cuenta->numero) ?></li>
</ol>

<div class="card">
<div class="card-header" >
           <legend><?= __('Nueva Cuenta') ?></legend>
    </div>
    <?= $this->Form->create($cuenta) ?>
<div class="card-block">
      <div class="form-group">
      <?php   echo $this->Form->control('numero', ['class' => 'form-control', 'label' => 'Número de Cuenta']); ?>
      </div>
      <div class="form-group">
        <label>Cliente(s)</label>
    <?php echo $this->Form->control('clientes._ids', ['id' => 'tokenize1', 'label' => false]); ?>
      </div>

        
        <?php    echo $this->Form->control('paise_id', ['type' => 'hidden']); ?>
       
         <div class="form-group">
        <?php    echo $this->Form->control('banco_id', ['options' => $bancos, 'class' => 'form-control', 'label' => 'Banco', 'empty' => 'Seleccione']); ?>
        </div>
        <div class="form-group">
        <?php    echo $this->Form->control('tipocuenta_id', ['options' => $tipocuentas, 'class' => 'form-control', 'label' => 'Tipo de cuenta', 'empty' => 'Seleccione']); ?>
        </div>
        <div class="form-group">
        <?php   echo $this->Form->control('moneda_id', ['options' => $monedas, 'class' => 'form-control', 'empty' => 'Seleccione']); ?>
        </div>
        <div class="form-group">
      <?php   echo $this->Form->control('compania_id', ['label' => 'Compañía administradora', 'options' => $companias, 'class' => 'form-control', 'empty' => 'Seleccione']); ?>
        </div>
       
        <?php    echo $this->Form->control('saldo', ['type' => 'hidden']); ?>
       
        <div class="form-group">
        <?php    echo $this->Form->control('tasa', ['class' => 'form-control', 'type' => 'decimal']); ?>
        </div>
        <div class="form-group">

        <?php    echo $this->Form->control('vencimiento', ['class' => 'form-control']); ?>
        </div>
        <div class="form-group">
        <?php    echo $this->Form->control('trust', ['class' => 'form-control']); ?>
        </div>
        <div class="form-group">
        <?php    echo $this->Form->control('plazo', ['class' => 'form-control', 'type' => 'text']); ?>
        </div>
        <div class="form-group">
        <?php    echo $this->Form->control('statucuenta_id', ['options' => $statucuentas, 'class' => 'form-control', 'label' => 'Status de la Cuenta']); ?>
        </div>

    <?= $this->Form->button(__('Guardar'), ['class' => 'btn btn-primary btn-lg']) ?>
    <?= $this->Form->end() ?>
</div>
</div>
