<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item"><a href="/cuentas">Cuentas</a> </li>
  <li class="breadcrumb-item active"><?= h($cuenta->numero) ?></li>
</ol>
<div class="card">
  <div class="card-block">
    <h3>Cuenta agregada N° <?= h($cuenta->id) ?></h3>
    <table class="table">

      <tr>
          <th scope="row"><?= __('N° de Cuenta') ?></th>
          <td><?= h($cuenta->numero) ?></td>
      </tr>
        <tr>
            <th scope="row"><?= __('Cliente(s)') ?></th>
            <td>
              <ul class="list-group" >
              <?php foreach ($cuenta->clientes as $clientes): ?>
              <li class="list-group-item"> <a href="/clientes/view/<?= h($clientes->id) ?>"> <?= h($clientes->nombre) ?></a></li>
               <?php endforeach; ?>
               </ul>

            </td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tipo de cuenta') ?></th>
            <td><?= $cuenta->has('tipocuenta') ? $this->Html->link($cuenta->tipocuenta->tipocuenta, ['controller' => 'Tipocuentas', 'action' => 'view', $cuenta->tipocuenta->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Moneda') ?></th>
            <td><?= $cuenta->has('moneda') ? $this->Html->link($cuenta->moneda->moneda, ['controller' => 'Monedas', 'action' => 'view', $cuenta->moneda->id]) : '' ?></td>
        </tr>

        <tr>
            <th scope="row"><?= __('Compañía') ?></th>
            <td><?= $cuenta->has('compania') ? $this->Html->link($cuenta->compania->nombre, ['controller' => 'Companias', 'action' => 'view', $cuenta->compania->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $cuenta->statucuenta->status ?></td>
        </tr>

        <tr>
            <th scope="row"><?= __('Saldo') ?></th>
            <td><?= $this->Number->precision($cuenta->get_saldo(), 2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tasa') ?></th>
            <td><?= $this->Number->precision($cuenta->tasa, 2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Vencimiento') ?></th>
            <td><?= h($cuenta->vencimiento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Plazo') ?></th>
            <td><?= h($cuenta->plazo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha de creación') ?></th>
            <td><?= h($cuenta->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Última modificación') ?></th>
            <td><?= h($cuenta->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Trust') ?></th>
            <td><?= $cuenta->admin ? __('Sí') : __('No'); ?></td>
        </tr>
    </table>
  </div>
</div>


<div class="card">
<div class="card-header">
<h3>Movimientos en esta cuenta</h3>
</div>
<div class="card-block">
<table class="table" id="table_id">
            <thead>

                <th><?= __('Fecha') ?></th>
                <th><?= __('Transacción') ?></th>
                <th><?= __('Monto') ?></th>
                <th><?= __('Descripción') ?></th>

            </thead>

                <?php foreach ($cuenta->movimientos as $movimientos): ?>
                <tr>
                                 <td>        <?= h($movimientos->created->format('d-m-Y')) ?></td>
                                 <td>        <?= $transaccion[$movimientos->tipotransacciones_id] ?></td>
                                 <td style="text-align: right;">

                                 <?php if ($movimientos->tipotransacciones_id == 1) { ?>

                                     <p style="color: green;">   <?= $this->Number->precision($movimientos->monto, 2) ?> </p>

                               <?php   } else { ?>

                                <p style="color: red;">     -<?= $this->Number->precision($movimientos->monto, 2) ?> </p>


                               <?php  } ?>






                                 </td>
                                 <td> <?= $movimientos->descripcion ?></td>

                </tr>
                                            <?php endforeach; ?>


</table>




</div>
</div>
