 <!-- Breadcrumb -->
 <ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item active">Cuentas</li>
</ol>
<?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
<ul class="nav">

        <li class="list-group-item"><?= $this->Html->link(__('Nueva Cuenta'), ['action' => 'add' ]) ?></li>


    </ul>

<?php endif; ?>
<div class="card">
 <div class="card-header">
    <h3><?= __('Cuentas') ?></h3>
    </div>
    <div class="card-block">
    <div class="table-responsive">
    <table class="table" id="cuentas">
        <thead>


                <th scope="col"><?= $this->Paginator->sort('cliente_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('numero', 'N° Cuenta') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tipocuenta_id', 'Tipo de cuenta') ?></th>
                <th scope="col"><?= $this->Paginator->sort('banco_id', 'Banco') ?></th>
                <th scope="col"><?= $this->Paginator->sort('moneda_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('saldo') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tasa') ?></th>

                <th scope="col"><?= $this->Paginator->sort('statucuenta_id', 'Status') ?></th>

                <th scope="col" class="actions"><?= __('Acciones') ?></th>

        </thead>
        <tbody>
            
            <?php foreach ($cuentas as $cuenta): ?>
            <tr>

                <td>
                  <ul class="list-group" style="text-align: right;">
                  <?php foreach ($cuenta->clientes as $clientes): ?>
                  <li class="list-group-item"> <a href="/clientes/view/<?= h($clientes->id) ?>"> <?= h($clientes->nombre) ?></a></li>
                   <?php endforeach; ?>
                   </ul>

                </td>
                <td><?= $cuenta->numero ?></td>
                <td><?= $cuenta->has('tipocuenta') ? $this->Html->link($cuenta->tipocuenta->tipocuenta, ['controller' => 'Tipocuentas', 'action' => 'view', $cuenta->tipocuenta->id]) : '' ?></td>
                <td><?= $cuenta->has('banco') ? $this->Html->link($cuenta->banco->nombre, ['controller' => 'Bancos', 'action' => 'view', $cuenta->banco->id]) : '' ?></td>
                <td><?= $cuenta->has('moneda') ? $this->Html->link($cuenta->moneda->moneda, ['controller' => 'Monedas', 'action' => 'view', $cuenta->moneda->id]) : '' ?></td>
                <td style="text-align: right;"><?= $this->Number->precision($cuenta->get_saldo(), 2) ?></td>
                <td style="text-align: right;"><?= $this->Number->precision($cuenta->tasa, 2) ?></td>


                <td><?= $cuenta->statucuenta->status ?></td>

                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $cuenta->id]) ?>
                    <?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $cuenta->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $cuenta->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cuenta->id)]) ?>
            <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>

    </div>
</div>
