<?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
<?php if ($operacione->cerrado == 0) { ?>
<ul class="side-nav">
        
<li class="list-group-item"><?= $this->Html->link(__('Editar'), ['action' => 'edit', $operacione->id]) ?> </li>
<li class="list-group-item"><?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $operacione->id], ['confirm' => __('Are you sure you want to delete # {0}?', $operacione->id)]) ?> </li>
       
    </ul>
<?php } else { ?>

<h3 style="color: green;"> SEMANA CERRADA </h3>



<?php } ?>
<?php endif; ?>
<div class="card">
<div class="card-header">
   <h3><?= __('Cierre Semanal') ?> N° <?= h($operacione->id) ?> </h3>
   </div>
   <div class="card-block">
    <table class="table">
        <tr>
            <th scope="row"><?= __('Descripción') ?></th>
            <td><?= h($operacione->descripcion) ?></td>
        </tr>
        <tr>
            <th scope="row">Creado por:</th>
            <td><?= $operacione->has('user') ? $this->Html->link($operacione->user->username, ['controller' => 'Users', 'action' => 'view', $operacione->user->id]) : '' ?></td>
        </tr>
       
        <tr>
            <th scope="row"><?= __('Fecha de creación') ?></th>
            <td><?= h($operacione->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Última modificación') ?></th>
            <td><?= h($operacione->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Movimientos') ?></h4>
        <?php if (!empty($operacione->movimientos)): ?>
        <table <table class="table" id="table_id">
        <thead>
            <tr>
               
             
               
                <th scope="col"><?= __('N° de cuenta') ?></th>
                <th scope="col"><?= __('Monto') ?></th>
                <th scope="col"><?= __('Tipo de transacción') ?></th>
                <th scope="col"><?= __('Fecha de creación') ?></th>
                <th scope="col"><?= __('Última modificación') ?></th>

                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
            </thead>
            <?php foreach ($operacione->movimientos as $movimientos): ?>
            <tr>
               
              
              
                <td><?= h($movimientos->cuenta->numero) ?></td>
                <td><?= h($movimientos->monto) ?></td>
                <td><?= h($movimientos->tipotransacciones_id) ?></td>
                <td><?= h($movimientos->created) ?></td>
                <td><?= h($movimientos->modified) ?></td>
             
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Movimientos', 'action' => 'view', $movimientos->id]) ?>
                    
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
