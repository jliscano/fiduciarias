<div class="operaciones form large-9 medium-8 columns content">








<?php if ($operacione->cerrado == 0) { ?>
  <div class="card">
    <div class="card-header">
       <h3>Agregar Movimiento para el <?= __('Batch') ?> N° <?= h($operacione->id) ?> </h3>
       </div>

        <div class="card-block">
      <?= $this->Form->create($operacione) ?>



    <div class="row">

    <?php echo $this->Form->input('movimientos.0.operacione_id', ['type' => 'hidden', 'value' => $operacione->id]); ?>

        <div class="form-group col-md-2">

        <?php echo $this->Form->input('cuentabancaria', ['label' => 'Cuenta Bancaria', 'type' => 'text', 'class' => 'form-control', 'id' => 'cuentasbancarias2']); ?>
        <?php echo $this->Form->control('movimientos.0.cuenta_id', ['type' => 'hidden']); ?>
    </div>
    <div class="form-group col-md-2">
        <?php echo $this->Form->control('movimientos.0.cliente_id', [  'Label'=> 'Cliente', 'class' => 'form-control']); ?>
        </div>
        <div class="form-group col-md-2">
        <?php echo $this->Form->input('banco', [ 'Label'=> 'Blanco', 'class' => 'form-control', 'type' => 'text', 'step' => 'any', 'readonly' => true]); ?>
        </div>
        <div class="form-group col-md-2">
        <?php echo $this->Form->input('moneda', [ 'Label'=> 'Moneda', 'class' => 'form-control', 'type' => 'text', 'step' => 'any', 'readonly' => true]); ?>
        </div>
        <div class="form-group col-md-2">
        <?php echo $this->Form->input('movimientos.0.monto', [ 'Label'=> 'Monto', 'class' => 'form-control', 'type' => 'text', 'step' => 'any', 'value' => '']); ?>
        </div>
        <div class="form-group col-md-2">

        <?php echo $this->Form->input('movimientos.0.tipotransacciones_id', ['label' => 'Tipo de transación',  'options' => $tipotransacciones, 'empty' => 'Seleccione', 'class' => 'form-control']); ?>
        </div>




        <div class="form-group col-8">

            <?php echo $this->Form->input('movimientos.0.descripcion', ['label' => 'Descripcion', 'class' => 'form-control']); ?>
            </div>
            <div class="form-group col-md-2">
                <div class="form-group">
                  <label for=""> Fecha </label>
                    <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                        <input type="text" name="movimientos[0][created]" class="form-control datetimepicker-input" data-target="#datetimepicker4"/>
                        <span class="input-group-addon" data-target="#datetimepicker4" data-toggle="datetimepicker">
                            <span class="fa fa-calendar"></span>
                        </span>

                    </div>
                </div>

            </div>
  <div class="form-group col-md-12">
          <?= $this->Form->button(__('Guardar'), ['class' => 'btn btn-primary']) ?>
          <?= $this->Form->end() ?>
        </div>
        </div>
      </div>
    </div>
<?php } else { ?>

<h3 style="color: green;"> BATCH CERRADO </h3>



<?php } ?>

<?php if ($operacione->cerrado == 0) { ?>

<?php } else { ?>
  <span class=""> Cerrado</span>
  <?php } ?>
    <div class="card">
      <div class="class-header">  <h4><?= __('Movimientos') ?></h4></div>
        <?php if (!empty($operacione->movimientos)): ?>
        <div class="card-block">

        <table  class="table" id="bacth2">
        <thead>
            <tr>



                <th scope="col"><?= __('N° de cuenta') ?></th>
                <th scope="col"><?= __('Cliente') ?></th>
                <th scope="col"><?= __('Banco') ?></th>
                <th scope="col"><?= __('Monto') ?></th>
                <th scope="col"><?= __('Tipo de transacción') ?></th>
                <th scope="col"><?= __('Descripción') ?></th>
                <th scope="col"><?= __('Fecha de creación') ?></th>
                <th scope="col"><?= __('Última modificación') ?></th>


                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
            </thead>
            <?php foreach ($operacione->movimientos as $movimientos): ?>
            <tr>



                <td><?= h($movimientos->cuenta->numero) ?></td>
                <td><?= h($movimientos->cliente->nombre) ?></td>
                <td><?= h($movimientos->cuenta->banco->nombre) ?></td>
                <td style="text-align: right;">
                  <?php if ($movimientos->tipotransaccione->id == 1) { ?>

                      <p style="color: green;">   <?= $this->Number->precision($movimientos->monto, 2) ?> </p>

                  <?php   } else { ?>

                  <p style="color: red;">     -<?= $this->Number->precision($movimientos->monto, 2) ?> </p>


                  <?php  } ?>



                </td>
                <td><?= h($movimientos->tipotransaccione->transaccion) ?></td>
                  <td><?= h($movimientos->descripcion) ?></td>
                <td><?= h($movimientos->created->format('d-m-Y')) ?></td>
                <td><?= h($movimientos->modified->format('d-m-Y')) ?></td>


                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Movimientos', 'action' => 'view', $movimientos->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'Movimientos', 'action' => 'edit', $movimientos->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
