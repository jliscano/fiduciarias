<link rel="stylesheet" type="text/css" href="/css/calendar/jscal/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="/css/calendar/jscal/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="/css/calendar/jscal/gray/gray.css" />
    <script type="text/javascript" src="/js/calendar/jscal/jscal2.js"></script>
    <script src="/js/calendar/jscal/lang/es.js"></script>
    <script src="/js/calendar/jscal/unicode-letter.js"></script>
	
<div class="operaciones form large-9 medium-8 columns content">
    <?= $this->Form->create($operacione) ?>
    <fieldset>
        <legend><?= __('Nuevo Cierre Semanal') ?></legend>


<div class="form-group">
        <?php echo $this->Form->control('descripcion', ['class' => 'form-control', 'label' => 'Descripción']); ?>
    </div>
    <div id="cont"></div>
	<div class="form-group">
        <?php echo $this->Form->control('fecha_desde', ['class' => 'form-control', 'label' => 'Desde ','type'=>'text','id'=>'fdesde']); ?>
    </div>
    <div class="form-group">
        <?php echo $this->Form->control('fecha_hasta', ['class' => 'form-control', 'label' => 'Hasta ','type'=>'text','id'=>'fhasta']); ?>
    </div>
    </fieldset>

    <table class="table" id="grade-table">
    <thead>
        <tr>

            <th></th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody></tbody>
    <tfoot>
        <tr>

            <td class="actions">
                <a href="#" class="add" title="Agregar"><div class="btn btn-success">  <i class="fa fa-plus-circle fa-lg"></i></div></a>
            </td>
        </tr>
    </tfoot>
</table>




<script id="grade-template" type="text/x-underscore-template">
<?php //echo $this->cell('Movimientos');?>
</script>


<?= $this->Form->button(__('Guardar'), ['class' => 'btn btn-primary btn-lg']) ?>
    <?= $this->Form->end() ?>
</div>
<script type="text/javascript">
						function primerUltimoDiaSemana(dia,mes,anio,sw){
				//lunes=0:martes=1:miercoles=2:jueves=3:viernes=4:sábado=5:domingo=6
				var suma=[5,4,3,2,1,0,6];
				var resta=[1,2,3,4,5,6,0];
				d = new Date(anio+'-'+mes+'-'+dia);
				var diapararestar = d.getDay();
				d.setHours(0, 0, 0, 0);
				if(sw==0)//calculas el primer día de la semana
					dias1=parseInt(dia) - resta[diapararestar];
				else
					dias1=parseInt(dia) + suma[diapararestar];
				d.setDate(dias1);
				return d;
			};
			//aqui hacer un query que traiga las semanas cerradas por cada mes
			var DISABLED_DATES = {
				20180602: true,
				20180605: true,
				20180610: true,
				20180611: true,
				20180710: true,
				20180811: true
			};
			var meses=['00','01','02','03','04','05','06','07','08','09','10','11','12'];
			var dias=['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'];
			var sw1=0;
            var LEFT_CAL = Calendar.setup({
                    cont: "cont",
                    fdow: 0,
                    weekNumbers: false,
                    selectionType: Calendar.SEL_MULTIPLE,
                    showTime: false,
                    checkRange: false,
                    disabled: function(date) {
						date = Calendar.dateToInt(date);
        				return date in DISABLED_DATES;
					},
                    onSelect      : function() {
							var data= this.selection.get()[0];
							var data2 =  data.toString();
							var ano = data2.substring(0,4);
							var mes = data2.substring(4,6);
							var dia = data2.substring(6,8);
							var pdia=primerUltimoDiaSemana(dia,mes,ano,0);
							document.getElementById('fdesde').value=pdia.getYear()+1900+'-'+meses[pdia.getMonth()]+'-'+dias[pdia.getDate()];
							var udia=primerUltimoDiaSemana(dia,mes,ano,1);
							document.getElementById('fhasta').value=udia.getYear()+1900+'-'+meses[udia.getMonth()]+'-'+dias[udia.getDate()];
						}
					,
					onChange    :function(date){
						return buscaSemanasCerradas(sw1);
					}
			});
			sw1=1;
			LEFT_CAL.addEventListener("onChange", buscaSemanasCerradas(sw1));
			function buscaSemanasCerradas(sw1){
				var date;
				if(sw1!=0){
					LEFT_CAL.redraw();
					return date in DISABLED_DATES;
					//alert(date);
				}
			};

          </script>
