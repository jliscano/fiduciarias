

<?php if ($operacione->cerrado == 0) { ?>
<ul class="side-nav">

<li class="list-group-item"><?= $this->Html->link(__('Agregar Movimiento'), ['action' => 'nuevomovimiento', $operacione->id]) ?> </li>


    </ul>
<?php } else { ?>

<h3 style="color: green;"> BATCH CERRADO </h3>



<?php } ?>
<div class="card">
<div class="card-header">
   <h3><?= __('Batch') ?> N° <?= h($operacione->id) ?> </h3>
   </div>
   <div class="card-block">
    <table class="table">

        <tr>
            <th scope="row">Creado por:</th>
            <td><?= $operacione->has('user') ? $this->Html->link($operacione->user->username, ['controller' => 'Users', 'action' => 'view', $operacione->user->id]) : '' ?></td>
        </tr>

        <tr>
            <th scope="row"><?= __('Fecha de creación') ?></th>
            <td><?= h($operacione->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Última modificación') ?></th>
            <td><?= h($operacione->modified) ?></td>
        </tr>
    </table>

</div>
</div>
<?php if ($operacione->cerrado == 0) { ?>

<?php } else { ?>
  <span class=""> Cerrado</span>
  <?php } ?>
    <div class="card">
      <div class="card-header">  <h4><?= __('Movimientos') ?></h4></div>
        <?php if (!empty($operacione->movimientos)): ?>
        <div class="card-block">
        <div class="table-responsive">
        <table  class="table" id="bacth2">
        <thead>
            <tr>



                <th scope="col"><?= __('N° de cuenta') ?></th>
                <th scope="col"><?= __('Cliente') ?></th>
                <th scope="col"><?= __('Banco') ?></th>
                <th scope="col"><?= __('Monto') ?></th>
                <th scope="col"><?= __('Tipo de transacción') ?></th>
                <th scope="col"><?= __('Descripción') ?></th>
                <th scope="col"><?= __('Fecha de creación') ?></th>
                <th scope="col"><?= __('Última modificación') ?></th>


                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
            </thead>
            <?php foreach ($operacione->movimientos as $movimientos): ?>
            <tr>



                <td><?= h($movimientos->cuenta->numero) ?></td>
                  <td><?= h($movimientos->cliente->nombre) ?></td>
                  <td><?= h($movimientos->cuenta->banco->nombre) ?></td>
                <td style="text-align: right;">

                  <?php if ($movimientos->tipotransaccione->id == 1) { ?>

                      <p style="color: green;">   <?= $this->Number->precision($movimientos->monto, 2) ?> </p>

                  <?php   } else { ?>

                  <p style="color: red;">     -<?= $this->Number->precision($movimientos->monto, 2) ?> </p>


                  <?php  } ?>


                </td>
                <td><?= h($movimientos->tipotransaccione->transaccion) ?></td>
                <td><?= h($movimientos->descripcion) ?></td>
                <td><?= h($movimientos->created->format('d-m-Y')) ?></td>
                <td><?= h($movimientos->modified->format('d-m-Y')) ?></td>


                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Movimientos', 'action' => 'view', $movimientos->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'Movimientos', 'action' => 'edit', $movimientos->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        </div>
        <?php endif; ?>
    </div>
</div>
