
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item"><a href="/operaciones/batch">Batch</a> </li>
  <li class="breadcrumb-item active">Nuevo movimiento en el Batch N° <?= h($operacione->id) ?>  </li>
</ol>
<?php if ($operacione->cerrado == 0) { ?>



<div class="card">
  <div class="card-header"> <h4>Nuevo movimiento</h4>
  </div>
  <div class="card-block">

<?= $this->Form->create(null) ?>
<div class="row">
      <?php echo $this->Form->control('operacione_id', [  'value' => $operacione->id, 'type' => 'hidden']); ?>

<div class="form-group col-3">
      <?php echo $this->Form->control('cuenta_id',  ['empty' => 'Seleccione', 'multiple' => false, 'options' => $cuentas, 'class' => 'form-control']); ?>
</div>
<div class="form-group col-3">
  <?php echo $this->Form->control('cliente_id', ['empty' => 'Seleccione', 'options' => $clientes,  'class' => 'form-control']); ?>

</div>
<div class="form-group col-3">
  <?php echo $this->Form->control('Banco', [ 'class' => 'form-control']); ?>

</div>
<div class="form-group col-3">
  <?php echo $this->Form->control('Moneda', [  'class' => 'form-control']); ?>

</div>
<div class="form-group col-3">
    <?php echo $this->Form->control('monto', [ 'class' => 'form-control']); ?>
    </div>
<div class="form-group col-3">
    <?php echo $this->Form->control('tipotransacciones_id', ['options' => $tipotransacciones, 'class' => 'form-control']); ?>
    </div>


    <div class="form-group col-3">
    <?php echo $this->Form->control('created', ['label' => 'Fecha', 'class' => 'form-control', 'id' => 'datepicker', 'type' => 'text']); ?>
    </div>

    </div>

<?= $this->Form->end() ?>


  </div>
</div>

<?php } else { ?>

<h3 style="color: green;"> BATCH CERRADO </h3>



<?php } ?>

<?php if ($operacione->cerrado == 0) { ?>

<?php } else { ?>
  <span class=""> Cerrado</span>
  <?php } ?>
    <div class="card">
      <div class="card-header">  <h4><?= __('Movimientos') ?></h4></div>
        <?php if (!empty($operacione->movimientos)): ?>
        <div class="card-block">
        <div class="table-responsive">
        <table  class="table" id="bacth2">
        <thead>
            <tr>



                <th scope="col"><?= __('N° de cuenta') ?></th>
                <th scope="col"><?= __('Cliente') ?></th>
                <th scope="col"><?= __('Banco') ?></th>
                <th scope="col"><?= __('Monto') ?></th>
                <th scope="col"><?= __('Tipo de transacción') ?></th>
                <th scope="col"><?= __('Descripción') ?></th>
                <th scope="col"><?= __('Fecha de creación') ?></th>
                <th scope="col"><?= __('Última modificación') ?></th>


                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
            </thead>
            <?php foreach ($operacione->movimientos as $movimientos): ?>
            <tr>



                <td><?= h($movimientos->cuenta->numero) ?></td>
                  <td><?= h($movimientos->cliente->nombre) ?></td>
                  <td><?= h($movimientos->cuenta->banco->nombre) ?></td>
                <td style="text-align: right;">

                  <?php if ($movimientos->tipotransaccione->id == 1) { ?>

                      <p style="color: green;">   <?= $this->Number->precision($movimientos->monto, 2) ?> </p>

                  <?php   } else { ?>

                  <p style="color: red;">     -<?= $this->Number->precision($movimientos->monto, 2) ?> </p>


                  <?php  } ?>


                </td>
                <td><?= h($movimientos->tipotransaccione->transaccion) ?></td>
                <td><?= h($movimientos->descripcion) ?></td>
                <td><?= h($movimientos->created->format('d-m-Y')) ?></td>
                <td><?= h($movimientos->modified->format('d-m-Y')) ?></td>


                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Movimientos', 'action' => 'view', $movimientos->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'Movimientos', 'action' => 'edit', $movimientos->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        </div>
        <?php endif; ?>
    </div>
</div>
