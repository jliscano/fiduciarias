

<ul class="nav">

<li class="list-group-item"><?= $this->Html->link(__('Nuevo movimiento'), ['action' => 'add']) ?></li>

    </ul>


    <div class="card">
    <div class="card-header">
       <h3><?= __('Batch') ?></h3>
       </div>
       <div class="card-block">
       <div class="table-responsive">
       <table class="table" id="table_id">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('descripcion') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id', 'Usuario') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cerrado') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created', 'Fecha de creación') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified', 'Ultima modificación') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($operaciones as $operacione): ?>
            <tr>
                <td><?= $this->Number->format($operacione->id) ?></td>
                <td><?= h($operacione->descripcion) ?></td>
                <td><?= $operacione->has('user') ? $this->Html->link($operacione->user->username, ['controller' => 'Users', 'action' => 'view', $operacione->user->id]) : '' ?></td>
                <td><?= $cerra[$operacione->cerrado] ?></td>
                <td><?= h($operacione->created) ?></td>
                <td><?= h($operacione->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $operacione->id]) ?>
                    <?php if ($operacione->cerrado == 0) { ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $operacione->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $operacione->id], ['confirm' => __('Are you sure you want to delete # {0}?', $operacione->id)]) ?>

                        <?php } else { ?> <em style="color: green;"> Cerrado </em> <?php }  ?>


                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
    </div>
     </div>
