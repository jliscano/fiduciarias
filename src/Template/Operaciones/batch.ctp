
<?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
<ul class="nav">

<li class="list-group-item"><?= $this->Form->postlink(__('Nuevo Batch'), ['action' => 'nuevobatch'], ['confirm' => __('¿Desea crear un nuevo batch ?')]) ?> </li>

    </ul>

<?php endif; ?>
    <div class="card">
    <div class="card-header">
       <h3><?= __('Batch') ?></h3>
       </div>
       <div class="card-block">
       <div class="table-responsive">
       <table class="table" id="batch">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>

                <th scope="col"><?= $this->Paginator->sort('cerrado', 'Status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created', 'Fecha de creación') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified', 'Ultima modificación') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($operaciones as $operacione): ?>
            <tr>
                <td><?= $this->Number->format($operacione->id) ?></td>

                <td><?= $cerra[$operacione->cerrado] ?></td>
                <td><?= h($operacione->created->format('d-m-Y')) ?></td>
                <td><?= h($operacione->modified->format('d-m-Y')) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $operacione->id]) ?>
                    <?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
                    <?php if ($operacione->cerrado == 0) { ?>
                    <?= $this->Html->link(__('Agregar movimiento'), ['action' => 'nuevomovimiento', $operacione->id]) ?>
                    <?= $this->Form->postLink(__('Cerrar'), ['action' => 'cerrarbatch', $operacione->id], ['confirm' => __('¿Desea cerrar el batch N° {0}?', $operacione->id)]) ?>


                        <?php } else { ?> <em style="color: green;"> Cerrado </em>
  <?= $this->Form->postLink(__('Reabrir'), ['action' => 'abrirbatch', $operacione->id], ['confirm' => __('¿Desea reabrir el batch N° {0}?', $operacione->id)]) ?>


                          <?php }  ?>

                            <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
    </div>
     </div>
