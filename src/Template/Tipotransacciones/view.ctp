<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tipotransaccione $tipotransaccione
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Tipotransaccione'), ['action' => 'edit', $tipotransaccione->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Tipotransaccione'), ['action' => 'delete', $tipotransaccione->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tipotransaccione->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Tipotransacciones'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tipotransaccione'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tipotransacciones view large-9 medium-8 columns content">
    <h3><?= h($tipotransaccione->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Transaccion') ?></th>
            <td><?= h($tipotransaccione->transaccion) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($tipotransaccione->id) ?></td>
        </tr>
    </table>
</div>
