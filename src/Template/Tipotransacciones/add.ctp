<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Tipotransacciones'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="tipotransacciones form large-9 medium-8 columns content">
    <?= $this->Form->create($tipotransaccione) ?>
    <fieldset>
        <legend><?= __('Add Tipotransaccione') ?></legend>
        <?php
            echo $this->Form->control('transaccion');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
