<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tipotransaccione[]|\Cake\Collection\CollectionInterface $tipotransacciones
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Tipotransaccione'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tipotransacciones index large-9 medium-8 columns content">
    <h3><?= __('Tipotransacciones') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('transaccion') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tipotransacciones as $tipotransaccione): ?>
            <tr>
                <td><?= $this->Number->format($tipotransaccione->id) ?></td>
                <td><?= h($tipotransaccione->transaccion) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $tipotransaccione->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $tipotransaccione->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $tipotransaccione->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tipotransaccione->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
