<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item"><a href="/bancos">Bancos</a> </li>
  <li class="breadcrumb-item active"> Nuevo</li>
</ol>
<div class="card">
<div class="card-header" >
           <legend><?= __('Nuevo Banco') ?></legend>
    </div>

    <?= $this->Form->create($banco) ?>
    <div class="card-block">
     <div class="form-group">
        <?php   echo $this->Form->control('nombre', ['class' => 'form-control']); ?>
        </div>
        <div class="form-group">
         <?php   echo $this->Form->control('paise_id', ['label' => 'País', 'options' => $paises, 'class' => 'form-control', 'empty' => 'Seleccione el país']); ?>
         </div>
         <div class="form-group">
           <?php echo $this->Form->control('direccion', ['class' => 'form-control']); ?>
           </div>

           <div class="form-group">
            <?php   echo $this->Form->control('telefono', ['class' => 'form-control', 'label' => 'Teléfono', 'type' => 'number']); ?>
          </div>
           <div class="form-group">

           <?php echo $this->Form->control( 'activo', ['type' => 'checkbox', 'class' => 'switch-input']); ?>


    </div>
   <?= $this->Form->button(__('Guardar'), ['class' => 'btn btn-primary btn-lg']) ?>
    <?= $this->Form->end() ?>

     </div>
</div>
