<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  
  <li class="breadcrumb-item active"> Bancos</li>
</ol>

 <?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
<ul class="nav">

        <li class="list-group-item"><?= $this->Html->link(__('Nuevo banco'), ['action' => 'add' ]) ?></li>


    </ul>
<?php endif; ?>
<div class="card">
 <div class="card-header">
    <h3><?= __('Bancos') ?></h3>
    </div>
    <div class="card-block">
    <div class="table-responsive">
    <table class="table" id="table_id">



        <thead>


                <th scope="col"><?= $this->Paginator->sort('nombre') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('telefono', 'Teléfono') ?></th>
                <th scope="col"><?= $this->Paginator->sort('paise_id', 'País') ?></th>
                <th scope="col"><?= $this->Paginator->sort('activo') ?></th>

                <th scope="col" class="actions"><?= __('Acciones') ?></th>

        </thead>
        <tbody>
            <?php foreach ($bancos as $banco): ?>
            <tr>
              
                <td><?= $banco->nombre ?></td>
                <td><?= $banco->telefono ?></td>
                <td><a href="/paises/view/<?= $banco->paise->id ?>"><?= $banco->paise->pais ?></a></td>
                <td><?= $activado[$banco->activo] ?></td>

                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $banco->id]) ?>
                    <?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $banco->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $banco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $banco->id)]) ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
    </div>
</div>
