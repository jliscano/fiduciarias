<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item"><a href="/bancos">Bancos</a> </li>
  <li class="breadcrumb-item active"><?= h($banco->nombre) ?></li>
</ol>
<?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
    <ul class="nav">
       
        <li><?= $this->Html->link(__('Editar'), ['action' => 'edit', $banco->id]) ?> </li>
        
    </ul>
<?php endif; ?>
</nav>
<div class="card">
<div class="card-header">
    <h3>Banco: <?= h($banco->nombre) ?></h3>
    </div>
    <div class="card-block">
    <table class="table">
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($banco->nombre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($banco->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('País') ?></th>
            <td><a href="/paises/view/<?= $banco->paise->id ?>"><?= $banco->paise->pais ?></a></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha de creación') ?></th>
            <td><?= h($banco->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Última modificación') ?></th>
            <td><?= h($banco->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Activo') ?></th>
            <td><?= $banco->activo ? __('Sí') : __('No'); ?></td>
        </tr>

        <tr>
            
        <th>Dirección</th>
        <td> <?= $this->Text->autoParagraph(h($banco->direccion)); ?></td>

        </tr>
    </table>
   </div>
</div>
