<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Provincia $provincia
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Provincia'), ['action' => 'edit', $provincia->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Provincia'), ['action' => 'delete', $provincia->id], ['confirm' => __('Are you sure you want to delete # {0}?', $provincia->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Provincias'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Provincia'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Clientes'), ['controller' => 'Clientes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cliente'), ['controller' => 'Clientes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="provincias view large-9 medium-8 columns content">
    <h3><?= h($provincia->provincia) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Provincia') ?></th>
            <td><?= h($provincia->provincia) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($provincia->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Paise Id') ?></th>
            <td><?= $this->Number->format($provincia->paise_id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Clientes') ?></h4>
        <?php if (!empty($provincia->clientes)): ?>
        <table class="table" id="table_id">
              <thead>
                <th ><?= __('Id') ?></th>
                <th ><?= __('Nombre') ?></th>
                <th ><?= __('Paise Id') ?></th>
                <th><?= __('Direccion') ?></th>
                <th ><?= __('Provincia Id') ?></th>
                <th ><?= __('Created') ?></th>
                <th ><?= __('Modified') ?></th>
                <th ><?= __('Actions') ?></th>
            </thead>
            <?php foreach ($provincia->clientes as $clientes): ?>
            <tr>
                <td><?= h($clientes->id) ?></td>
                <td><?= h($clientes->nombre) ?></td>
                <td><?= h($clientes->paise_id) ?></td>
                <td><?= h($clientes->direccion) ?></td>
                <td><?= h($clientes->provincia_id) ?></td>
                <td><?= h($clientes->created) ?></td>
                <td><?= h($clientes->modified) ?></td>
                <td>
                    <?= $this->Html->link(__('View'), ['controller' => 'Clientes', 'action' => 'view', $clientes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Clientes', 'action' => 'edit', $clientes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Clientes', 'action' => 'delete', $clientes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $clientes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
