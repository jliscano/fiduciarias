




   <div class="card">
 <div class="card-header">
    <h3><?= __('Clientes') ?></h3>
    </div>
    <div class="card-block">
    <table class="table" id="table_id">
        <thead>
          
               
                <th scope="col"><?= $this->Paginator->sort('paise_id', 'País') ?></th>
                <th scope="col"><?= $this->Paginator->sort('provincia') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
           
        </thead>
        <tbody>
            <?php foreach ($provincias as $provincia): ?>
            <tr>
               
                <td><?= $provincia->paise->pais ?></td>
                <td><?= h($provincia->provincia) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $provincia->id]) ?>
                    
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
    </div>