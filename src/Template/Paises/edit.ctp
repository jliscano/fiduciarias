<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item"><a href="/paises">Países</a> </li>
  <li class="breadcrumb-item active"> Editar: <?= h($paise->pais) ?></li>
</ol>

<div class="card">
  <div class="card-header">
  <legend><?= __('Editar País') ?></legend>
  </div>
  <div class="card-block">
    <?= $this->Form->create($paise) ?>

       
        <div class="form-group">
        <?php echo $this->Form->control('pais', ['class' => 'form-control']); ?>
        </div>
        
             <?= $this->Form->button(__('Editar'), ['class' => 'btn btn-primary btn-lg']) ?>
    <?= $this->Form->end() ?>
  </div>
</div>
