<!-- Breadcrumb -->
<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">Países</li>
            </ol>
<ul class="nav">

<li class="list-group-item"><?= $this->Html->link(__('Nuevo país'), ['action' => 'add' ]) ?></li>


</ul>


<div class="card">
 <div class="card-header">
    <h3>Países</h3>
    </div>
    <div class="card-block">
    <table class="table" id="table_id">
        <thead>
            <tr>
             
                <th scope="col"><?= $this->Paginator->sort('pais') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($paises as $paise): ?>
            <tr>
                
                <td><?= h($paise->pais) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $paise->id]) ?>
                    <?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $paise->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $paise->id], ['confirm' => __('¿Desea borrar # {0}?', $paise->id)]) ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
   </div>
</div>
