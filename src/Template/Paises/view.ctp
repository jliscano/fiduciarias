<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Home</a> </li>
  <li class="breadcrumb-item"><a href="/paises">Países</a> </li>
  <li class="breadcrumb-item active">  <?= h($paise->pais) ?></li>
</ol>
<div class="card">
  
<div class="card-header">
      <h3><?= h($paise->pais) ?></h3>
    </div>
    <div class="card-block">



    <table class="table">
        <tr>
            <th scope="row"><?= __('Pais') ?></th>
            <td><?= h($paise->pais) ?></td>
        </tr>
       
    </table>
    </div>
    </div>
        <?php if (!empty($paise->clientes)): ?>
        <div class="card">
 <div class="card-header">
    <h3>Clientes en <?= h($paise->pais) ?></h3>
    </div>
    <div class="card-block">
    <table class="table" id="table_id">
            <thead>
                
                <th><?= __('Nombre') ?></th>
                
            </thead>
            <?php foreach ($paise->clientes as $cliente): ?>
            <tr>
             
                <td><a href="/clientes/view/<?= h($cliente->id) ?>"><?= h($cliente->nombre) ?></a></td>
             
                
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
