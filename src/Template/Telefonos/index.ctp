
<div class="card">
 <div class="card-header">
    <h3><?= __('Teléfonos') ?></h3>
    </div>
    <div class="card-block">
    <table class="table" id="table_id">
        <thead>
            <tr>
               <th><?= $this->Paginator->sort('cliente_id') ?></th>
                <th><?= $this->Paginator->sort('telefono') ?></th>
               
              
            </tr>
        </thead>
        <tbody>
            <?php foreach ($telefonos as $telefono): ?>
            <tr>
                <td><a href="/clientes/view/<?= h($telefono->cliente->id) ?>"><?= h($telefono->cliente->nombre) ?></a></td>
                <td><?= h($telefono->telefono) ?></td>
                
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
 
   </div>
</div>
