<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Statucuenta $statucuenta
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Statucuenta'), ['action' => 'edit', $statucuenta->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Statucuenta'), ['action' => 'delete', $statucuenta->id], ['confirm' => __('Are you sure you want to delete # {0}?', $statucuenta->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Statucuentas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Statucuenta'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cuentas'), ['controller' => 'Cuentas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cuenta'), ['controller' => 'Cuentas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="statucuentas view large-9 medium-8 columns content">
    <h3><?= h($statucuenta->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($statucuenta->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($statucuenta->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Cuentas') ?></h4>
        <?php if (!empty($statucuenta->cuentas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Cliente Id') ?></th>
                <th scope="col"><?= __('Tipocuenta Id') ?></th>
                <th scope="col"><?= __('Moneda Id') ?></th>
                <th scope="col"><?= __('Saldo') ?></th>
                <th scope="col"><?= __('Tasa') ?></th>
                <th scope="col"><?= __('Plazo') ?></th>
                <th scope="col"><?= __('Admin') ?></th>
                <th scope="col"><?= __('Statucuenta Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($statucuenta->cuentas as $cuentas): ?>
            <tr>
                <td><?= h($cuentas->id) ?></td>
                <td><?= h($cuentas->cliente_id) ?></td>
                <td><?= h($cuentas->tipocuenta_id) ?></td>
                <td><?= h($cuentas->moneda_id) ?></td>
                <td><?= h($cuentas->saldo) ?></td>
                <td><?= h($cuentas->tasa) ?></td>
                <td><?= h($cuentas->plazo) ?></td>
                <td><?= h($cuentas->admin) ?></td>
                <td><?= h($cuentas->statucuenta_id) ?></td>
                <td><?= h($cuentas->created) ?></td>
                <td><?= h($cuentas->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Cuentas', 'action' => 'view', $cuentas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Cuentas', 'action' => 'edit', $cuentas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Cuentas', 'action' => 'delete', $cuentas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cuentas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
