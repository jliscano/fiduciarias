<?php
$key = isset($key) ? $key : '<%= key %>';
?>


  <tr>
    <td>
   <div class="form-row">

    <div class="form-group col-md-3">
    <?php echo $this->Form->input("movimientos.{$key}.id", ['type' => 'hidden']) ?>
  
        <?php echo $this->Form->control("movimientos.{$key}.cuenta_id", ['class' => 'form-control', 'options' => $cuentas, 'label' => 'N° de cuenta', 'empty' => 'Seleccione...']); ?>
    </div>
    <div class="form-group col-md-3">    
        <?php echo $this->Form->control("movimientos.{$key}.cliente", ['class' => 'form-control']); ?>
        </div>
        <div class="form-group col-md-3">    
        <?php echo $this->Form->control("movimientos.{$key}.banco", ['class' => 'form-control']); ?>
        </div>
    <div class="form-group col-md-3">    
        <?php echo $this->Form->control("movimientos.{$key}.monto", [ 'class' => 'form-control']); ?>
        </div>
        <div class="form-group col-md-3">    
        <?php echo $this->Form->input("movimientos.{$key}.created", [ 'label'=> 'Fecha', 'type' => 'text', 'class' => 'form-control', 'id' => 'datepicker0']); ?>
        </div>
       
        
    <div class="form-group col-md-3">
        <?php echo $this->Form->control("movimientos.{$key}.tipotransacciones_id", ['options' => $tipotransacciones, 'class' => 'form-control', 'label' => 'Tipo de transacción']); ?>
        </div>
        <div class="form-group col-md-6">    
        <?php echo $this->Form->control("movimientos.{$key}.descripcion", [ 'class' => 'form-control']); ?>
        </div>
        </td>
        </div>
       
    <td class="actions">
        <a href="#" class="remove" title="Quitar"><div class="btn btn-danger"><i class="fa fa-minus-circle fa-lg"> </i></div></a>
    </td>
</tr>

   
