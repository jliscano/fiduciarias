<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item active ">Nombres de Clientes</li>

</ol>
<?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
<ul class="nav">
        <li class="list-group-item"><?= $this->Html->link(__('Nuevo nombre de cliente'), ['action' => 'add']) ?></li>
        </ul>
<?php endif; ?>
<div class="card">
 <div class="card-header">
    <h3><?= __('Nombres de Clientes') ?></h3>
    </div>
    <div class="card-block">
    <table class="table" id="table_id">
        <thead>
            <tr>

                <th><?= $this->Paginator->sort('nombre') ?></th>



                <th><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($personas as $persona): ?>
            <tr>

                <td><?= h($persona->nombre) ?></td>



                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $persona->id]) ?>
                    <?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $persona->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $persona->id], ['confirm' => __('Are you sure you want to delete # {0}?', $persona->id)]) ?>
                    <?php endif; ?>
                </td>
            </tr>
             <?php endforeach; ?>
        </tbody>
    </table>
   </div>
</div>
