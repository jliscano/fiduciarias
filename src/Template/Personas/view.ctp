<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item"><a href="/personas">Nombres de clientes</a></li>
<li class="breadcrumb-item active"><?= h($persona->nombre) ?> </li>
</ol>
<?php if ($tipodeusuario == "admin" or $tipodeusuario == "operador"): ?>
    <ul class="nav">

        <li class="list-group-item"><?= $this->Html->link(__('Editar'), ['action' => 'edit', $persona->id]) ?> </li>

    </ul>
<?php endif; ?>
<div class="card">
<div class="card-header">
    <h3><?= h($persona->nombre) ?></h3>
    </div>

    <div class="card-block">
    <table class="table">

    <tr>
            <th scope="row"><?= __('Cliente(s)') ?></th>
            <td>

 <ul class="list-group">
              <?php foreach ($persona->clientes as $cliente): ?>
              <li class="list-group-item"> <a href="/clientes/view/<?= h($cliente->id) ?>" > <?= h($cliente->nombre) ?></a></li>
              <?php endforeach; ?>
</ul>


            </td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($persona->nombre) ?></td>
        </tr>



        <tr>
            <th scope="row"><?= __('Fecha de creación') ?></th>
            <td><?= h($persona->created->format('d-m-Y')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Última modificación') ?></th>
            <td><?= h($persona->modified->format('d-m-Y')) ?></td>
        </tr>
    </table>
    </div>
    </div>
