<?php
$key = isset($key) ? $key : '<%= key %>';
?>
<tr>
    <td>
    <div class="form-group">
        <?php echo $this->Form->input("telefonos.{$key}.id") ?>
        <?php echo $this->Form->text("telefonos.{$key}.telefono", ["class" => "form-control", "placeholder" => "Telefono"]); ?>
        </div>
    </td>

    <td class="actions">
        <a href="#" class="remove" title="Quitar"><div class="btn btn-danger"><i class="fa fa-minus-circle fa-lg"> </i></div></a>
    </td>
</tr>
