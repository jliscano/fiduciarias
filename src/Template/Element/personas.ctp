<?php
$key = isset($key) ? $key : '<%= key %>';
?>
<tr>
    <td>
    <div class="form-group">
        <?php echo $this->Form->input("personas.{$key}.id") ?>
        <?php echo $this->Form->text("personas.{$key}.nombre", ["class" => "form-control", "placeholder" => "Nombre"]); ?>
         <?php echo $this->Form->text("personas.{$key}.cedula", ["class" => "form-control", "placeholder" => "Cédula o Pasaporte"]); ?>
        <?php echo $this->Form->text("personas.{$key}.email", ["class" => "form-control", "placeholder" => "Correo electrónico"]); ?>
        </div>
    </td>

    <td class="actions">
        <a href="#" class="remove" title="Quitar"><div class="btn btn-danger"><i class="fa fa-minus-circle fa-lg"> </i></div></a>
    </td>
</tr>
