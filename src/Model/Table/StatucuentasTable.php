<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Statucuentas Model
 *
 * @property \App\Model\Table\CuentasTable|\Cake\ORM\Association\HasMany $Cuentas
 *
 * @method \App\Model\Entity\Statucuenta get($primaryKey, $options = [])
 * @method \App\Model\Entity\Statucuenta newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Statucuenta[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Statucuenta|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Statucuenta patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Statucuenta[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Statucuenta findOrCreate($search, callable $callback = null, $options = [])
 */
class StatucuentasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('statucuentas');
        $this->setDisplayField('status');
        $this->setPrimaryKey('id');

        $this->hasMany('Cuentas', [
            'foreignKey' => 'statucuenta_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }
}
