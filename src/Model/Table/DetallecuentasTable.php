<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Detallecuentas Model
 *
 * @method \App\Model\Entity\Detallecuenta get($primaryKey, $options = [])
 * @method \App\Model\Entity\Detallecuenta newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Detallecuenta[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Detallecuenta|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Detallecuenta patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Detallecuenta[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Detallecuenta findOrCreate($search, callable $callback = null, $options = [])
 */
class DetallecuentasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('detallecuentas');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('numero')
            ->allowEmpty('numero');

        $validator
            ->scalar('nombre')
            ->requirePresence('nombre', 'create')
            ->notEmpty('nombre');

        return $validator;
    }
}
