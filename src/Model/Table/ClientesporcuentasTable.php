<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Clientesporcuentas Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Cuentas
 * @property |\Cake\ORM\Association\BelongsTo $Clientes
 *
 * @method \App\Model\Entity\Clientesporcuenta get($primaryKey, $options = [])
 * @method \App\Model\Entity\Clientesporcuenta newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Clientesporcuenta[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Clientesporcuenta|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Clientesporcuenta patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Clientesporcuenta[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Clientesporcuenta findOrCreate($search, callable $callback = null, $options = [])
 */
class ClientesporcuentasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('clientesporcuentas');
        $this->setDisplayField('nombrecliente');
        $this->setPrimaryKey('id');

        $this->belongsTo('Cuentas', [
            'foreignKey' => 'cuenta_id'
        ]);
        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->requirePresence('id', 'create')
            ->notEmpty('id');

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 120)
            ->requirePresence('nombre', 'create')
            ->notEmpty('nombre');

        $validator
            ->integer('activo')
            ->allowEmpty('activo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cuenta_id'], 'Cuentas'));
        $rules->add($rules->existsIn(['cliente_id'], 'Clientes'));

        return $rules;
    }
}
