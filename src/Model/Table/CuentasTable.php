<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cuentas Model
 *
 * @property \App\Model\Table\ClientesTable|\Cake\ORM\Association\BelongsTo $Clientes
 * @property \App\Model\Table\TipocuentasTable|\Cake\ORM\Association\BelongsTo $Tipocuentas
 * @property \App\Model\Table\MonedasTable|\Cake\ORM\Association\BelongsTo $Monedas
 * @property \App\Model\Table\StatucuentasTable|\Cake\ORM\Association\BelongsTo $Statucuentas
 *
 * @method \App\Model\Entity\Cuenta get($primaryKey, $options = [])
 * @method \App\Model\Entity\Cuenta newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Cuenta[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Cuenta|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cuenta patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Cuenta[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Cuenta findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CuentasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cuentas');
        $this->setDisplayField('numero');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Clientes', [
            'joinTable' => 'cuentas_clientes',
            'through' => 'CuentasClientes',
        ]);

        


        $this->belongsTo('Tipocuentas', [
            'foreignKey' => 'tipocuenta_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Monedas', [
            'foreignKey' => 'moneda_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Paises', [
            'foreignKey' => 'paise_id',
            'joinType' => 'INNER'
        ]);
         $this->belongsTo('Bancos', [
            'foreignKey' => 'banco_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Statucuentas', [
            'foreignKey' => 'statucuenta_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Companias', [
            'foreignKey' => 'compania_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('Movimientos', [
            'foreignKey' => 'cuenta_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->decimal('saldo')
            ->requirePresence('saldo', 'create')
            ->notEmpty('saldo');

        $validator
            ->decimal('tasa')
            ->requirePresence('tasa', 'create')
            ->notEmpty('tasa');



        $validator
            ->boolean('trust')
            ->requirePresence('trust', 'create')
            ->notEmpty('trust');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cliente_id'], 'Clientes'));
        $rules->add($rules->existsIn(['tipocuenta_id'], 'Tipocuentas'));
        $rules->add($rules->existsIn(['moneda_id'], 'Monedas'));
        $rules->add($rules->existsIn(['statucuenta_id'], 'Statucuentas'));

        return $rules;
    }
}
