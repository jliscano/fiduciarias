<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Clientesdetallados Model
 *
 * @property \App\Model\Table\CuentasTable|\Cake\ORM\Association\BelongsTo $Cuentas
 * @property \App\Model\Table\ClientesTable|\Cake\ORM\Association\BelongsTo $Clientes
 *
 * @method \App\Model\Entity\Clientesdetallado get($primaryKey, $options = [])
 * @method \App\Model\Entity\Clientesdetallado newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Clientesdetallado[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Clientesdetallado|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Clientesdetallado patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Clientesdetallado[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Clientesdetallado findOrCreate($search, callable $callback = null, $options = [])
 */
class ClientesdetalladosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('clientesdetallados');
        $this->setDisplayField('nombrecliente');
        $this->setPrimaryKey('id');

        $this->belongsTo('Cuentas', [
            'foreignKey' => 'cuenta_id'
        ]);
        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->requirePresence('id', 'create')
            ->notEmpty('id');

        $validator
            ->scalar('nombrecliente')
            ->maxLength('nombrecliente', 120)
            ->requirePresence('nombrecliente', 'create')
            ->notEmpty('nombrecliente');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cuenta_id'], 'Cuentas'));
        $rules->add($rules->existsIn(['cliente_id'], 'Clientes'));

        return $rules;
    }
}
