<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tipocuentas Model
 *
 * @property \App\Model\Table\CuentasTable|\Cake\ORM\Association\HasMany $Cuentas
 *
 * @method \App\Model\Entity\Tipocuenta get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tipocuenta newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tipocuenta[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tipocuenta|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tipocuenta patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tipocuenta[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tipocuenta findOrCreate($search, callable $callback = null, $options = [])
 */
class TipocuentasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tipocuentas');
        $this->setDisplayField('tipocuenta');
        $this->setPrimaryKey('id');

        $this->hasMany('Cuentas', [
            'foreignKey' => 'tipocuenta_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('tipocuenta', 'create')
            ->notEmpty('tipocuenta');

        return $validator;
    }
}
