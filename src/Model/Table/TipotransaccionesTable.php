<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tipotransacciones Model
 *
 * @method \App\Model\Entity\Tipotransaccione get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tipotransaccione newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tipotransaccione[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tipotransaccione|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tipotransaccione patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tipotransaccione[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tipotransaccione findOrCreate($search, callable $callback = null, $options = [])
 */
class TipotransaccionesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tipotransacciones');
        $this->setDisplayField('transaccion');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('transaccion')
            ->requirePresence('transaccion', 'create')
            ->notEmpty('transaccion');

        return $validator;
    }
}
