<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Cuenta Entity
 *
 * @property int $id
 * @property int $cliente_id
 * @property int $tipocuenta_id
 * @property int $moneda_id
 * @property float $saldo
 * @property float $tasa
 * @property \Cake\I18n\FrozenDate $plazo
 * @property bool $admin
 * @property int $statucuenta_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Cliente $cliente
 * @property \App\Model\Entity\Tipocuenta $tipocuenta
 * @property \App\Model\Entity\Moneda $moneda
 * @property \App\Model\Entity\Statucuenta $statucuenta
 */
class Cuenta extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    public function get_saldo() {
        $value = 0;
        foreach ($this->movimientos as $mov) {
            if ($mov->tipotransacciones_id == 1) {
                $value = $value + $mov->monto;
            } else if ($mov->tipotransacciones_id == 2) {
                $value = $value - $mov->monto;
            }
        }
        return $value;
    }

}
