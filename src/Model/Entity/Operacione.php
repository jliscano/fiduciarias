<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Operacione Entity
 *
 * @property int $id
 * @property string $descripción
 * @property string $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Movimiento[] $movimientos
 */
class Operacione extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'descripcion' => true,
        'cerrado' => true,
        'user_id' => true,
        'tipooperacione_id' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'movimientos' => true,
        'clientes' => true

    ];
}
