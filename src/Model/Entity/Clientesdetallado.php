<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Clientesdetallado Entity
 *
 * @property int $id
 * @property int $cuenta_id
 * @property int $cliente_id
 * @property string $nombrecliente
 *
 * @property \App\Model\Entity\Cuenta $cuenta
 * @property \App\Model\Entity\Cliente $cliente
 */
class Clientesdetallado extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'cuenta_id' => true,
        'cliente_id' => true,
        'nombrecliente' => true,
        'cuenta' => true,
        'cliente' => true
    ];
}
