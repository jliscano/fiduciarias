<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CuentasCliente Entity
 *
 * @property int $id
 * @property int $cuenta_id
 * @property int $cliente_id
 *
 * @property \App\Model\Entity\Cuenta $cuenta
 * @property \App\Model\Entity\Cliente $cliente
 */
class CuentasCliente extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'cuenta_id' => true,
        'cliente_id' => true,
        'cuenta' => true,
        'cliente' => true
    ];
    public function get_saldo($movimientos) {
        $value = 0;
        foreach ($movimientos as $mov) {
            if ($mov->tipotransacciones_id == 1) {
                $value = $value + $mov->monto;
            } else if ($mov->tipotransacciones_id == 2) {
                $value = $value - $mov->monto;
            }
        }
        return $value;
    }
}
