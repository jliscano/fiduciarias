<?php
namespace App\Test\TestCase\Controller;

use App\Controller\OperacionesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\OperacionesController Test Case
 */
class OperacionesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.operaciones',
        'app.users',
        'app.movimientos',
        'app.clientes',
        'app.paises',
        'app.bancos',
        'app.cuentas',
        'app.tipocuentas',
        'app.monedas',
        'app.statucuentas',
        'app.companias',
        'app.provincias',
        'app.telefonos',
        'app.personas',
        'app.tipotransacciones'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
