<?php
namespace App\Test\TestCase\Controller;

use App\Controller\StatucuentasController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\StatucuentasController Test Case
 */
class StatucuentasControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.statucuentas',
        'app.cuentas',
        'app.clientes',
        'app.paises',
        'app.bancos',
        'app.provincias',
        'app.personas',
        'app.clientes_personas',
        'app.telefonos',
        'app.personas_telefonos',
        'app.clientes_telefonos',
        'app.tipocuentas',
        'app.monedas'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
