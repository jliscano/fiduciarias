<?php
namespace App\Test\TestCase\Controller;

use App\Controller\TelefonosController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\TelefonosController Test Case
 */
class TelefonosControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.telefonos',
        'app.clientes',
        'app.paises',
        'app.bancos',
        'app.provincias',
        'app.cuentas',
        'app.tipocuentas',
        'app.monedas',
        'app.statucuentas',
        'app.personas',
        'app.clientes_personas',
        'app.personas_telefonos',
        'app.clientes_telefonos'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
