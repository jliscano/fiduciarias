<?php
namespace App\Test\TestCase\Controller;

use App\Controller\CuentasController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\CuentasController Test Case
 */
class CuentasControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cuentas',
        'app.clientes',
        'app.paises',
        'app.provincias',
        'app.personas',
        'app.clientes_personas',
        'app.telefonos',
        'app.clientes_telefonos',
        'app.tipocuentas',
        'app.monedas',
        'app.statucuentas'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
