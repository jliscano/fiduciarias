<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PersonasController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\PersonasController Test Case
 */
class PersonasControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.personas',
        'app.clientes',
        'app.paises',
        'app.bancos',
        'app.provincias',
        'app.cuentas',
        'app.tipocuentas',
        'app.monedas',
        'app.statucuentas',
        'app.clientes_personas',
        'app.telefonos',
        'app.clientes_telefonos',
        'app.personas_telefonos'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
