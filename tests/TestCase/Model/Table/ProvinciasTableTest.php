<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProvinciasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProvinciasTable Test Case
 */
class ProvinciasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProvinciasTable
     */
    public $Provincias;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.provincias',
        'app.clientes',
        'app.paises',
        'app.bancos',
        'app.cuentas',
        'app.tipocuentas',
        'app.monedas',
        'app.statucuentas',
        'app.personas',
        'app.clientes_personas',
        'app.telefonos',
        'app.personas_telefonos',
        'app.clientes_telefonos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Provincias') ? [] : ['className' => ProvinciasTable::class];
        $this->Provincias = TableRegistry::get('Provincias', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Provincias);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
