<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TipocuentasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TipocuentasTable Test Case
 */
class TipocuentasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TipocuentasTable
     */
    public $Tipocuentas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tipocuentas',
        'app.cuentas',
        'app.clientes',
        'app.paises',
        'app.bancos',
        'app.provincias',
        'app.personas',
        'app.clientes_personas',
        'app.telefonos',
        'app.clientes_telefonos',
        'app.personas_telefonos',
        'app.monedas',
        'app.statucuentas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Tipocuentas') ? [] : ['className' => TipocuentasTable::class];
        $this->Tipocuentas = TableRegistry::get('Tipocuentas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Tipocuentas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
