<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClientesdetalladosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClientesdetalladosTable Test Case
 */
class ClientesdetalladosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ClientesdetalladosTable
     */
    public $Clientesdetallados;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.clientesdetallados',
        'app.cuentas',
        'app.clientes',
        'app.paises',
        'app.bancos',
        'app.provincias',
        'app.telefonos',
        'app.personas',
        'app.clientes_personas',
        'app.cuentas_clientes',
        'app.tipocuentas',
        'app.monedas',
        'app.statucuentas',
        'app.companias',
        'app.movimientos',
        'app.tipotransacciones',
        'app.operaciones',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Clientesdetallados') ? [] : ['className' => ClientesdetalladosTable::class];
        $this->Clientesdetallados = TableRegistry::get('Clientesdetallados', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Clientesdetallados);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
