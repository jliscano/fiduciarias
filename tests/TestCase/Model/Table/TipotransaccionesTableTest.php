<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TipotransaccionesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TipotransaccionesTable Test Case
 */
class TipotransaccionesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TipotransaccionesTable
     */
    public $Tipotransacciones;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tipotransacciones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Tipotransacciones') ? [] : ['className' => TipotransaccionesTable::class];
        $this->Tipotransacciones = TableRegistry::get('Tipotransacciones', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Tipotransacciones);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
