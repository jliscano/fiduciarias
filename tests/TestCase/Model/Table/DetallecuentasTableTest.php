<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DetallecuentasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DetallecuentasTable Test Case
 */
class DetallecuentasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DetallecuentasTable
     */
    public $Detallecuentas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.detallecuentas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Detallecuentas') ? [] : ['className' => DetallecuentasTable::class];
        $this->Detallecuentas = TableRegistry::get('Detallecuentas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Detallecuentas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
