<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StatucuentasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StatucuentasTable Test Case
 */
class StatucuentasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StatucuentasTable
     */
    public $Statucuentas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.statucuentas',
        'app.cuentas',
        'app.clientes',
        'app.paises',
        'app.bancos',
        'app.provincias',
        'app.personas',
        'app.clientes_personas',
        'app.telefonos',
        'app.personas_telefonos',
        'app.clientes_telefonos',
        'app.tipocuentas',
        'app.monedas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Statucuentas') ? [] : ['className' => StatucuentasTable::class];
        $this->Statucuentas = TableRegistry::get('Statucuentas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Statucuentas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
