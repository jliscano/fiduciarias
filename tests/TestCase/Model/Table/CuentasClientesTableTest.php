<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CuentasClientesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CuentasClientesTable Test Case
 */
class CuentasClientesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CuentasClientesTable
     */
    public $CuentasClientes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cuentas_clientes',
        'app.cuentas',
        'app.clientes',
        'app.paises',
        'app.bancos',
        'app.provincias',
        'app.telefonos',
        'app.personas',
        'app.clientes_personas',
        'app.tipocuentas',
        'app.monedas',
        'app.statucuentas',
        'app.companias',
        'app.movimientos',
        'app.tipotransacciones',
        'app.operaciones',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CuentasClientes') ? [] : ['className' => CuentasClientesTable::class];
        $this->CuentasClientes = TableRegistry::get('CuentasClientes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CuentasClientes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
