<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClientesporcuentasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClientesporcuentasTable Test Case
 */
class ClientesporcuentasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ClientesporcuentasTable
     */
    public $Clientesporcuentas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.clientesporcuentas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Clientesporcuentas') ? [] : ['className' => ClientesporcuentasTable::class];
        $this->Clientesporcuentas = TableRegistry::get('Clientesporcuentas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Clientesporcuentas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
